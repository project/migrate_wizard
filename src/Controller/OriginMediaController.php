<?php

namespace Drupal\migrate_wizard\Controller;

use Drupal\migrate_wizard\MWManageDataService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A controller to interact with origin Media entities.
 */
class OriginMediaController extends MwControllerBase {

  /**
   * The MWManageDataService.
   *
   * @var \Drupal\migrate_wizard\MWManageDataService
   */
  protected $mwManageDataService;

  /**
   * Constructs a OriginMediaController object.
   *
   * @param \Drupal\migrate_wizard\MWManageDataService $mw_manage_data_service
   *   The MWManageDataService.
   */
  public function __construct(MWManageDataService $mw_manage_data_service) {
    $this->mwManageDataService = $mw_manage_data_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('migrate_wizard.migrate_wizard_manage_data_service'),
    );
  }

  /**
   * Call MWManageDataService for clear Origin Medias.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to the list of origin Medias.
   */
  public function clearOriginMedias() {
    $mwDatabase = $this->mwManageDataService->clearOriginMedias();
    $this->messenger()->addStatus($this->t('Completed clear Origin Medias.'));

    return $this->redirect('entity.origin_medias.list', ['mw_database' => $mwDatabase->id]);
  }

  /**
   * Call MWManageDataService for read Origin Medias.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to the list of origin Medias.
   */
  public function readOriginMedias() {
    $mwDatabase = $this->mwManageDataService->readOriginMedias();
    $this->messenger()->addStatus($this->t('Completed discovery for Origin Medias.'));

    return $this->redirect('entity.origin_medias.list', ['mw_database' => $mwDatabase->id]);
  }

}
