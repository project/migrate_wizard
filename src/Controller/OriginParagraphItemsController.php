<?php

namespace Drupal\migrate_wizard\Controller;

use Drupal\migrate_wizard\MWManageDataService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A controller to interact with origin paragraphs entities.
 */
class OriginParagraphItemsController extends MwControllerBase {

  /**
   * The MWManageDataService.
   *
   * @var \Drupal\migrate_wizard\MWManageDataService
   */
  protected $mwManageDataService;

  /**
   * Constructs a OriginParagraphItemsController object.
   *
   * @param \Drupal\migrate_wizard\MWManageDataService $mw_manage_data_service
   *   The MWManageDataService.
   */
  public function __construct(MWManageDataService $mw_manage_data_service) {
    $this->mwManageDataService = $mw_manage_data_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('migrate_wizard.migrate_wizard_manage_data_service'),
    );
  }

  /**
   * Call MWManageDataService for clear Origin Paragraphs.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to the list of origin paragraphs.
   */
  public function clearOriginParagraphs() {
    $mwDatabase = $this->mwManageDataService->clearOriginParagraphs();
    $this->messenger()->addStatus($this->t('Completed clear Origin Paragraphs.'));

    return $this->redirect('entity.origin_paragraph.list', ['mw_database' => $mwDatabase->id]);
  }

  /**
   * Call MWManageDataService for read Origin Paragraphs.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to the list of origin paragraphs.
   */
  public function readOriginParagraphs() {
    $mwDatabase = $this->mwManageDataService->readOriginParagraphs();
    $this->messenger()->addStatus($this->t('Completed discovery for Origin Paragraphs.'));

    return $this->redirect('entity.origin_paragraph.list', ['mw_database' => $mwDatabase->id]);
  }

}
