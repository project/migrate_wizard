<?php

namespace Drupal\migrate_wizard\Controller;

use Drupal\migrate_wizard\MWManageDataService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A controller to interact with origin vocabulary entities.
 */
class OriginVocabularyController extends MwControllerBase {

  /**
   * The MWManageDataService.
   *
   * @var \Drupal\migrate_wizard\MWManageDataService
   */
  protected $mwManageDataService;

  /**
   * Constructs a OriginVocabularyController object.
   *
   * @param \Drupal\migrate_wizard\MWManageDataService $mw_manage_data_service
   *   The MWManageDataService.
   */
  public function __construct(MWManageDataService $mw_manage_data_service) {
    $this->mwManageDataService = $mw_manage_data_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('migrate_wizard.migrate_wizard_manage_data_service'),
    );
  }

  /**
   * Call MWManageDataService for clear Origin Vocabularies.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to the list of Origin Vocabularies.
   */
  public function clearOriginVocabularies() {
    $mwDatabase = $this->mwManageDataService->clearOriginVocabularies();
    $this->messenger()->addStatus($this->t('Completed clear Origin Vocabularies.'));

    return $this->redirect('entity.origin_vocabularies.list', ['mw_database' => $mwDatabase->id]);
  }

  /**
   * Call MWManageDataService for read Origin Vocabularies.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to the list of Origin Vocabularies.
   */
  public function readOriginVocabularies() {
    $mwDatabase = $this->mwManageDataService->readOriginVocabularies();
    $this->messenger()->addStatus($this->t('Completed discovery for Origin Vocabularies.'));

    return $this->redirect('entity.origin_vocabularies.list', ['mw_database' => $mwDatabase->id]);
  }

}
