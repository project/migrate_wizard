<?php

namespace Drupal\migrate_wizard\Controller;

use Drupal\migrate_wizard\MWManageDataService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A controller to interact with origin field collection entities.
 */
class OriginFieldCollectionItemController extends MwControllerBase {

  /**
   * The MWManageDataService.
   *
   * @var \Drupal\migrate_wizard\MWManageDataService
   */
  protected $mwManageDataService;

  /**
   * Constructs a OriginFieldCollectionItemController object.
   *
   * @param \Drupal\migrate_wizard\MWManageDataService $mw_manage_data_service
   *   The MWManageDataService.
   */
  public function __construct(MWManageDataService $mw_manage_data_service) {
    $this->mwManageDataService = $mw_manage_data_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('migrate_wizard.migrate_wizard_manage_data_service'),
    );
  }

  /**
   * Call MWManageDataService for clear Origin Field Collection Items.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to the list of origin field collections.
   */
  public function clearOriginFieldCollectionItems() {
    $mwDatabase = $this->mwManageDataService->clearOriginFieldCollectionItems();
    $this->messenger()->addStatus($this->t('Completed clear Origin Field Collections.'));

    return $this->redirect('entity.origin_field_collection.list', ['mw_database' => $mwDatabase->id]);
  }

  /**
   * Call MWManageDataService for read Origin Field Collection Items.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to the list of origin field collections.
   */
  public function readOriginFieldCollectionItems() {
    $mwDatabase = $this->mwManageDataService->readOriginFieldCollectionItems();
    $this->messenger()->addStatus($this->t('Completed discovery for Origin Field Collections.'));

    return $this->redirect('entity.origin_field_collection.list', ['mw_database' => $mwDatabase->id]);
  }

}
