<?php

namespace Drupal\migrate_wizard;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Manage the Form methods.
 *
 * @property array arrayFieldsOriginBundle
 */
class MWManageFormService {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  public $moduleHandler;

  /**
   * The current Path stack.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  public $pathCurrent;

  /**
   * The array of fields from the origin bundle.
   *
   * @var array
   */
  protected $arrayFieldsOriginBundle;

  /**
   * The config from form.
   *
   * @var array
   */
  protected $config;

  /**
   * The current configuration of migration.
   *
   * @var array
   */
  protected $currentConfig;

  /**
   * The fields from the origin bundle.
   *
   * @var array
   */
  protected $fieldsOriginBundle;

  /**
   * The current database.
   *
   * @var array
   */
  protected $mwDatabase;

  /**
   * The Mw Config Builder service.
   *
   * @var \Drupal\migrate_wizard\MWConfigBuilder
   */
  protected MWConfigBuilder $mwConfigBuilder;

  /**
   * The Mw Manage Data Service.
   *
   * @var \Drupal\migrate_wizard\MWManageDataService
   */
  protected MWManageDataService $mwManageDataService;

  /**
   * The entity fields destiny.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface[]|mixed
   */
  private mixed $entityFieldsDestiny = [];

  use \Drupal\Core\StringTranslation\StringTranslationTrait;

  /**
   * MWManageFormService constructor.
   *
   * @param \Drupal\migrate_wizard\MWManageDataService $mw_manage_data_service
   *   The manage data service.
   * @param \Drupal\migrate_wizard\MWConfigBuilder $mw_config_builder
   *   The config builder service.
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   *   The module handler.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Path\CurrentPathStack $path_current
   *   The current path stack.
   */
  public function __construct(MWManageDataService $mw_manage_data_service,
                              MWConfigBuilder $mw_config_builder,
                              ModuleHandler $module_handler,
                              RequestStack $request_stack,
                              CurrentPathStack $path_current) {
    $this->mwManageDataService = $mw_manage_data_service;
    $this->mwConfigBuilder = $mw_config_builder;
    $this->moduleHandler = $module_handler;
    $this->mwDatabase = $request_stack->getCurrentRequest()->get('mw_database');
    $this->pathCurrent = $path_current;
  }

  /**
   * Build the checkboxes on form.
   *
   * @param array $form
   *   The form.
   * @param array $checkboxes
   *   The checkboxes.
   */
  public function buildCheckboxes(&$form, $checkboxes): void {
    foreach ($checkboxes as $key => $label) {
      $default_value = FALSE;

      if ($this->currentConfig && isset($this->currentConfig[$key])) {
        $default_value = $this->currentConfig[$key];
      }
      $form[$key] = [
        '#type' => 'checkbox',
        '#title' => $label,
        '#default_value' => $default_value,
      ];
    }
  }

  /**
   * Build the fields on form.
   *
   * @param array $form
   *   The form.
   * @param array $current_options_source_destiny_fields
   *   The current options source destiny fields.
   * @param string $generic_current_source_type_value
   *   The generic current source type value.
   * @param array $both_options_source
   *   The both options source.
   * @param array $current_config_node
   *   The current config node.
   * @param array $fields
   *   The fields.
   * @param int $depth
   *   The depth.
   * @param array $children_settings
   *   The children settings.
   * @param string $parent_prefix
   *   The parent prefix.
   */
  public function buildFieldsForm(&$form,
                                  $current_options_source_destiny_fields,
                                  $generic_current_source_type_value,
                                  $both_options_source,
                                  $current_config_node,
                                  $fields,
                                  $depth = 0,
                                  $children_settings = [],
                                  $parent_prefix = ''): void {
    foreach ($fields as $field) {
      // @todo refactor phpstan phpcs unserialize
      $current_data = unserialize($field['data'], ['allowed_classes' => ['stdClass']]);
      $current_fc_data = unserialize($field['fc_data'], ['allowed_classes' => ['stdClass']]);
      $depth_indent = $depth * 50;
      $depth_indent_extra = $depth_indent + 50;
      $field_type = $field['type'];
      if ($field_type === 'datetime' && \count($current_fc_data['indexes']) > 1) {
        $field_type = 'daterange';
      }
      $current_value = $this->currentConfig !== NULL && isset($current_config_node[$field['field_name']]) ? $current_config_node[$field['field_name']]['destiny'] : 'none';
      if ($depth > 0) {
        $current_value = 'none';
        if (isset($children_settings['children'][$field['field_name']]['destiny'])) {
          $current_value = $children_settings['children'][$field['field_name']]['destiny'];
        }
      }
      $form['source_destiny_fields'][$field['field_name']] = [
        '#type' => 'select',
        '#title' => $current_data['label'] . ' - ' . $field['field_name'] . ' (' . $field_type . ')',
        '#options' => $current_options_source_destiny_fields,
        '#validated' => TRUE,
        '#attributes' => [
          'type' => $field_type,
          'style' => 'margin-left: ' . $depth_indent . 'px;',
        ],
        '#label_attributes' => ['style' => 'margin-left: ' . $depth_indent . 'px;'],
        '#value' => $current_value,
      ];
      // If type field is datetime add format to transform.
      if ($field_type === 'datetime' || $field_type === 'daterange') {
        $form['source_destiny_fields'][$field['field_name']]['#attributes']['format'] = TRUE;
        $form['source_destiny_fields'][$field['field_name']]['#prefix'] = '<div class="datetime_mw">';
        $form['source_destiny_fields'][$field['field_name']]['#suffix'] = '</div>';
        $form['source_destiny_fields'][$field['field_name'] . '/format'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Destiny Format Y-m-d H:i:s'),
          '#validated' => TRUE,
          '#value' => $this->currentConfig !== NULL && isset($current_config_node[$field['field_name']]['format']) ? $current_config_node[$field['field_name']]['format'] : '',
          '#attributes' => ['style' => 'margin-left: ' . $depth_indent . 'px;'],
          '#label_attributes' => ['style' => 'margin-left: ' . $depth_indent . 'px;'],
          '#prefix' => '<div class="format_date_mw">',
          '#suffix' => '</div>',
        ];
      }
      if ($this->moduleHandler->moduleExists('media')) {
        if ($field_type === 'file' || $field_type === 'image') {
          $media_types = ['none' => 'Select media type'] + $this->mwManageDataService->getMediaTypes();
          $current_value_media_type = $this->currentConfig !== NULL && isset($this->currentConfig[$generic_current_source_type_value][$field['field_name']]['media_type']) ? $this->currentConfig[$generic_current_source_type_value][$field['field_name']]['media_type'] : NULL;
          $form['source_destiny_fields']['listmediatypes/' . $field['field_name']] = [
            '#type' => 'select',
            '#title' => $this->t('Select media type (only if destiny is media entity)'),
            '#options' => $media_types,
            '#ajax' => [
              'callback' => [$this, 'getFieldsMedia'],
              'event' => 'change',
              'wrapper' => 'media-type-wrapper',
            ],
            '#validated' => TRUE,
            '#attributes' => [
              'type' => $field['field_name'],
              'style' => 'margin-left: ' . $depth_indent_extra . 'px;',
              'destiny_ajax_callback' => 'media_fields_box_' . $field['field_name'],
            ],
            '#label_attributes' => ['style' => 'margin-left: ' . $depth_indent_extra . 'px;'],
            '#value' => $current_value_media_type,
          ];
          $current_value_field_media = $this->currentConfig !== NULL && isset($this->currentConfig[$generic_current_source_type_value][$field['field_name']]['media_field']) ? $this->currentConfig[$generic_current_source_type_value][$field['field_name']]['media_field'] : NULL;
          $fields_media = $this->mwManageDataService->getFieldsMediaByBundle($current_value_media_type);

          // @todo refactor - use custom attributes instead listmediafields/
          $form['source_destiny_fields']['listmediafields/' . $field['field_name']] = [
            '#type' => 'select',
            '#title' => $this->t('Select field media (only if destiny is media entity)'),
            '#options' => $fields_media,
            '#validated' => TRUE,
            '#attributes' => ['style' => 'margin-left: ' . $depth_indent_extra . 'px;'],
            '#label_attributes' => ['style' => 'margin-left: ' . $depth_indent_extra . 'px;'],
            '#value' => $current_value_field_media,
            '#prefix' => '<div class="media_fields_box_' . $field['field_name'] . '">',
            '#suffix' => '</div>',
          ];
        }
      }

      if ($field_type === 'entityreference' || $field_type === 'taxonomy_term_reference' || $field_type === 'node_reference') {
        $target_bundles = [];
        if ($field_type === 'entityreference') {
          $target_bundles = $current_fc_data['settings']['handler_settings']['target_bundles'];
        }
        if ($field_type === 'taxonomy_term_reference') {
          $target_bundles = [$current_fc_data['settings']['allowed_values'][0]['vocabulary'] => $current_fc_data['settings']['allowed_values'][0]['vocabulary']];
        }
        if ($field_type === 'node_reference') {
          foreach ($current_fc_data['settings']['referenceable_types'] as $key => $value) {
            if (\is_string($value)) {
              $target_bundles[$key] = $value;
            }
          }
        }
        $form['source_destiny_fields'][$field['field_name']]['#attributes']['target_type'] = $current_fc_data["settings"]["target_type"] ?? NULL;
        $form['source_destiny_fields'][$field['field_name']]['#attributes']['dependencies'] = $target_bundles;
        foreach ($target_bundles as $key => $value) {
          $current_value = [];
          $reset_current_config = [];
          if ($this->currentConfig !== 0) {
            $reset_current_config = reset($this->currentConfig);
          }
          if ($reset_current_config !== 0 && isset($reset_current_config[$field['field_name']]['dependencies'][$key])) {
            $current_value = $reset_current_config[$field['field_name']]['dependencies'][$key];
          }
          $form['source_destiny_fields'][$field['field_name'] . '/' . $key] = [
            '#type' => 'select',
            '#title' => 'Select dependency ' . $key,
            '#options' => $both_options_source,
            '#validated' => TRUE,
            '#attributes' => ['style' => 'margin-left: ' . $depth_indent_extra . 'px;'],
            '#label_attributes' => ['style' => 'margin-left: ' . $depth_indent_extra . 'px;'],
            '#value' => $current_value,
          ];
        }
      }

      if ($field_type === 'field_collection') {
        if (isset($current_config_node[$field['field_name']])) {
          $children_settings = $current_config_node[$field['field_name']];
        }
        elseif (isset($children_settings['children'][$field['field_name']])) {
          $children_settings = $children_settings['children'][$field['field_name']];
        }
        $current_value = 'none';
        if (isset($children_settings['target_bundles'])) {
          $current_value = $children_settings['target_bundles'];
        }
        $bundles_paragraph = ['none' => 'Select target paragraph'] + $this->mwManageDataService->getParagraphBundles();
        $form['source_destiny_fields'][$field['field_name'] . '/target_bundle'] = [
          '#type' => 'select',
          '#title' => 'Select target bundle dependency (lookup)',
          '#options' => $bundles_paragraph,
          '#validated' => TRUE,
          '#value' => $current_value,
          '#attributes' => ['style' => 'margin-left: 50px;'],
          '#label_attributes' => ['style' => 'margin-left: 50px;'],
        ];
        $form['source_destiny_fields'][$field['field_name']]['#prefix'] = '<div class="field_collection">';
        $form['source_destiny_fields'][$field['field_name']]['#suffix'] = '</div>';
        if ($depth === 0) {
          $children_settings = [];
        }
      }

      if ($field_type === 'paragraphs') {
        if (isset($current_config_node[$field['field_name']])) {
          $children_settings = $current_config_node[$field['field_name']];
        }
        elseif (isset($children_settings['children'][$field['field_name']])) {
          $children_settings = $children_settings['children'][$field['field_name']];
        }
        $current_value = 'none';
        if (isset($children_settings['target_bundles'])) {
          $current_value = $children_settings['target_bundles'];
        }
        $bundles_paragraph = ['none' => 'Select target paragraph'] + $this->mwManageDataService->getParagraphBundles();
        $form['source_destiny_fields'][$field['field_name'] . '/target_bundle'] = [
          '#type' => 'select',
          '#title' => 'Select target bundle dependency (lookup)',
          '#options' => $bundles_paragraph,
          '#validated' => TRUE,
          '#value' => $current_value,
          '#attributes' => ['style' => 'margin-left: 50px;'],
          '#label_attributes' => ['style' => 'margin-left: 50px;'],
        ];
        $form['source_destiny_fields'][$field['field_name']]['#prefix'] = '<div class="field_collection">';
        $form['source_destiny_fields'][$field['field_name']]['#suffix'] = '</div>';

      }
      $this->arrayFieldsOriginBundle[$field['field_name']] = $field_type;
    }
  }

  /**
   * Build the form langcodes.
   *
   * Builds the checkboxes for the translations, if the variable $i18nMode
   * is not null, it is because we are in a vocabulary and we save the
   * value in a hidden input to take it into account when generating
   * the migration yml.
   *
   * @param array $form
   *   The form.
   * @param array $langCodes
   *   The langcodes.
   * @param int $i18nMode
   *   The i18n mode.
   */
  public function buildLangcodes(&$form, $langCodes, $i18nMode): void {
    foreach ($langCodes as $key_langcode => $langcode) {
      $default_langcode_value = FALSE;
      $disabled = FALSE;
      $checked = FALSE;
      if ($key_langcode === 'default_lang') {
        $default_langcode_value = TRUE;
        $disabled = TRUE;
        $checked = TRUE;
      }
      else {
        if ($this->currentConfig && isset(reset($this->currentConfig)['languages'][$langcode['value']]) && reset($this->currentConfig)['languages'][$langcode['value']]) {
          $default_langcode_value = TRUE;
          $checked = TRUE;
        }
      }
      $form['languages'][$key_langcode] = [
        '#type' => 'checkbox',
        '#title' => $langcode['markup'] . ' (' . $langcode['value'] . ')',
        '#default_value' => $default_langcode_value,
        '#disabled' => $disabled,
        '#checked' => $checked,
      ];
    }
    if ($i18nMode !== NULL) {
      $form['i18n_mode'] = [
        '#type' => 'hidden',
        '#value' => $i18nMode,
      ];
      switch ($i18nMode) {
        case 1:
          $form['i18n_mode_message'] = [
            '#markup' => '<div class="i18n_mode_message">' . $this->t('The i18n_mode configuration is: "Localize. Terms are common for all languages, but their name and description may be localized."') . '</div>',
          ];
          break;

        case 2:
          foreach ($langCodes as $language) {
            unset($form['languages'][$language['value'] . '_lang']);
          }
          $form['languages']['default_lang'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Default translation') . ' (' . $langCodes['default_lang']['value'] . ')',
            '#default_value' => TRUE,
            '#disabled' => TRUE,
            '#checked' => TRUE,
          ];
          $form['i18n_mode_message'] = [
            '#markup' => '<div class="i18n_mode_message">' . $this->t('The i18n_mode configuration is: "Fixed Language. Terms will have a global language and they will only show up for pages in that language."') . '</div>',
          ];
          break;

        case 4:
          $form['i18n_mode_message'] = [
            '#markup' => '<div class="i18n_mode_message">' . $this->t('The i18n_mode configuration is: "Translate. Different terms will be allowed for each language and they can be translated."') . '</div>',
          ];
          break;
      }
    }
  }

  /**
   * Build the selectors for the migration destinations.
   *
   * @param array $form
   *   The form.
   * @param string $current_source_type_value
   *   The current source type value.
   * @param string $current_source_type_vocabulary_value
   *   The current source type vocabulary value.
   * @param string $current_source_type_paragraph_value
   *   The current source type paragraph value.
   */
  public function buildSourceTypesForm(&$form,
                                       &$current_source_type_value,
                                       &$current_source_type_vocabulary_value,
                                       &$current_source_type_paragraph_value): void {
    $form['source_destiny_bundle_fields'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Select a bundle destiny type'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#prefix' => '<div class="source-destiny-bundle-fields-wrapper" id="source-destiny-bundle-fields-wrapper">',
      '#suffix' => '</div>',
    ];
    $options_source_type = $this->mwManageDataService->getCurrentContentTypes();
    $options_source_type_vocabulary = $this->mwManageDataService->getCurrentVocabularies();
    if ($this->moduleHandler->moduleExists('paragraphs')) {
      $options_source_type_paragraph = $this->mwManageDataService->getParagraphBundles();
    }
    $form['source_destiny_bundle_fields']['source_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Content types'),
      '#options' => ['none' => 'Select content type'] + $options_source_type,
      '#ajax' => [
        'callback' => [$this, 'getFieldsDestiny'],
        'event' => 'change',
        'wrapper' => 'source-destiny-fields-wrapper',
      ],
      '#value' => $current_source_type_value = ($this->currentConfig === 0 ? 'none' : array_key_first($this->currentConfig)),
      '#attributes' => ['destiny_type' => 'node'],
      '#prefix' => '<div class="source_type_box">',
      '#suffix' => '</div>',
    ];
    $form['source_destiny_bundle_fields']['source_type_vocabulary'] = [
      '#type' => 'select',
      '#title' => $this->t('Vocabularies'),
      '#options' => ['none' => 'Select vocabulary'] + $options_source_type_vocabulary,
      '#ajax' => [
        'callback' => [$this, 'getFieldsDestiny'],
        'event' => 'change',
      ],
      '#value' => $current_source_type_vocabulary_value = ($this->currentConfig === 0 ? 'none' : array_key_first($this->currentConfig)),
      '#attributes' => ['destiny_type' => 'taxonomy_term'],
      '#prefix' => '<div class="source_type_vocabulary_box">',
      '#suffix' => '</div>',
    ];
    if (isset($options_source_type_paragraph) && $options_source_type_paragraph !== NULL) {
      $form['source_destiny_bundle_fields']['source_type_paragraph'] = [
        '#type' => 'select',
        '#title' => $this->t('Paragraphs'),
        '#options' => ['none' => 'Select paragraph'] + $options_source_type_paragraph,
        '#ajax' => [
          'callback' => [$this, 'getFieldsDestiny'],
          'event' => 'change',
        ],
        '#value' => $current_source_type_paragraph_value = ($this->currentConfig === 0 ? 'none' : array_key_first($this->currentConfig)),
        '#attributes' => ['destiny_type' => 'paragraph'],
        '#prefix' => '<div class="source_type_paragraph_box">',
        '#suffix' => '</div>',
      ];
    }
    $current_source_type_date_field = '';
    if ($this->currentConfig && isset($this->currentConfig['source_type_date_field'])) {
      $current_source_type_date_field = $this->currentConfig['source_type_date_field'];
    }
    $form['source_type_date_field'] = [
      '#type' => 'date',
      '#title' => $this->t('Import from date'),
      '#date_date_format' => 'd/m/Y',
      '#default_value' => date('d/m/Y'),
      '#value' => $current_source_type_date_field,
    ];
  }

  /**
   * Build the selectors for the media migration destinations.
   *
   * @param array $form
   *   The form.
   * @param string $current_source_type_value
   *   The current source type value.
   */
  public function buildSourceTypesMediasForm(&$form, &$current_source_type_value): void {
    $options_source_type = $this->mwManageDataService->getMediaTypes();

    $form['source_type_media'] = [
      '#type' => 'select',
      '#title' => $this->t('Medias'),
      '#options' => ['none' => 'Select Media'] + $options_source_type,
      '#ajax' => [
        'callback' => [$this, 'getFieldsMediaDestiny'],
        'event' => 'change',
        'wrapper' => 'source-destiny-fields-wrapper',
      ],
      '#value' => $current_source_type_value = ($this->currentConfig === NULL ? 'none' : array_key_first($this->currentConfig)),
      '#attributes' => [
        'destiny_type' => 'media',
        'destiny_ajax_callback' => 'media_fields_box',
      ],
      '#prefix' => '<div class="source_type_box">',
      '#suffix' => '</div>',
    ];

    $form['source_destiny_fields']['listmediafields'] = [
      '#type' => 'select',
      '#title' => $this->t('Select field media'),
      '#options' => $this->mwManageDataService->getFieldsMediaByBundle($current_source_type_value),
      '#validated' => TRUE,
      '#value' => $this->currentConfig['listmediafields'] ?? NULL,
      '#prefix' => '<div class="media_fields_box">',
      '#suffix' => '</div>',
    ];
  }

  /**
   * Builds the selectors for the paragraph migration targets.
   *
   * @param array $form
   *   The form.
   * @param string $current_source_type_value
   *   The current source type value.
   */
  public function buildSourceTypesParagraphsForm(&$form, &$current_source_type_value): void {
    $options_source_type = $this->mwManageDataService->getParagraphBundles();

    $form['source_type_paragraph'] = [
      '#type' => 'select',
      '#title' => $this->t('Paragraphs'),
      '#options' => ['none' => 'Select paragraph'] + $options_source_type,
      '#ajax' => [
        'callback' => [$this, 'getFieldsDestiny'],
        'event' => 'change',
        'wrapper' => 'source-destiny-fields-wrapper',
      ],
      '#value' => $current_source_type_value = ($this->currentConfig === 0 ? 'none' : array_key_first($this->currentConfig)),
      '#attributes' => ['destiny_type' => 'paragraph'],
      '#prefix' => '<div class="source_type_box">',
      '#suffix' => '</div>',
    ];
  }

  /**
   * Build the part of form for workbench access.
   *
   * @param array $form
   *   The form.
   * @param array $current_options_source_destiny_fields_array
   *   The current options source destiny fields array.
   * @param array $current_config_node
   *   The current config node.
   * @param array $both_options_source
   *   The both options source.
   */
  public function buildWorkbenchAccessForm(&$form, $current_options_source_destiny_fields_array, $current_config_node, $both_options_source): void {
    if ($this->mwManageDataService->checkWorkbenchAccessNodeExist()) {
      $form['source_destiny_fields']['workbench_access_node'] = [
        '#type' => 'select',
        '#title' => 'workbench access node - workbench_access_node (workbench_access_node)',
        '#options' => $current_options_source_destiny_fields_array['fields_array'],
        '#validated' => TRUE,
        '#value' => $this->currentConfig !== NULL && isset($current_config_node['workbench_access_node']['destiny']) ? $current_config_node['workbench_access_node']['destiny'] : 'none',
      ];

      $form['source_destiny_fields']['workbench_access_node/dependency/workbench_access_node'] = [
        '#type' => 'select',
        '#title' => 'select dependency workbench access node',
        '#options' => $both_options_source,
        '#validated' => TRUE,
        '#attributes' => ['style' => 'margin-left: 50px;'],
        '#value' => $this->currentConfig !== NULL && isset($current_config_node['workbench_access_node']['dependencies']['workbench_access_node']) ? $current_config_node['workbench_access_node']['dependencies']['workbench_access_node'] : 'none',
      ];
    }
  }

  /**
   * Get the destiny fields by bundle.
   *
   * Builds selector options when entering the configuration of an existing
   * migration or when changing a migration destination (via ajax)
   *
   * @param string $type
   *   The type of the destiny entity.
   * @param string $bundle
   *   The bundle of the destiny entity.
   *
   * @return array
   *   The fields array.
   */
  public function getDestinyFieldsByBundle($type, $bundle): array {
    if ($bundle !== 'none') {
      $this->entityFieldsDestiny = $this->mwManageDataService->entityFieldManager->getFieldDefinitions($type, $bundle);
    }
    else {
      $this->entityFieldsDestiny = NULL;
    }
    if ($bundle === 'user') {
      unset($this->entityFieldsDestiny['name']);
    }
    $fields_array = [];
    $fields_array_destiny_type = [];
    if ($this->entityFieldsDestiny !== NULL) {
      foreach ($this->entityFieldsDestiny as $field_name => $field) {
        $label = $field->getLabel();

        if ($label instanceof TranslatableMarkup) {
          $label = $label->getUntranslatedString();
        }
        $field_name_explode = explode('/', $field_name);
        if (\count($field_name_explode) > 1) {
          $label = $label . ' (' . $field_name_explode[1] . ')';
        }
        $fields_array[$field_name] = $label;
        $fields_array_destiny_type[$field->getName()] = $field->getType();
      }
    }
    ksort($fields_array);
    $fields_array = ['none' => 'Select a field'] + $fields_array;
    return [
      'fields_array' => $fields_array,
      'fields_array_destiny_type' => $fields_array_destiny_type,
    ];
  }

  /**
   * Get the fields destiny.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   */
  public function getFieldsDestiny(array &$form, FormStateInterface $form_state): AjaxResponse {
    $user_input = $form_state->getUserInput();
    $triggering_element = $form_state->getTriggeringElement();
    $response = new AjaxResponse();
    $bundle = '';

    // In each case, the selected value is set to the form fields and
    // none to the rest.
    switch ($triggering_element['#attributes']['destiny_type']) {
      case 'node':
        $bundle = $user_input['source_type'];
        $form['destiny_type']['#value'] = 'node';
        $form['source_destiny_bundle_fields']['source_type']['#value'] = $bundle;
        $form['source_destiny_bundle_fields']['source_type_vocabulary']['#value'] = 'none';
        $form['source_destiny_bundle_fields']['source_type_paragraph']['#value'] = 'none';
        $response->addCommand(new ReplaceCommand('.source-destiny-bundle-fields-wrapper', $form['source_destiny_bundle_fields']));
        break;

      case 'taxonomy_term':
        $bundle = $user_input['source_type_vocabulary'];
        $form['destiny_type']['#value'] = 'taxonomy_term';
        $form['source_destiny_bundle_fields']['source_type_vocabulary']['#value'] = $bundle;
        $form['source_destiny_bundle_fields']['source_type']['#value'] = 'none';
        $form['source_destiny_bundle_fields']['source_type_paragraph']['#value'] = 'none';
        $response->addCommand(new ReplaceCommand('.source-destiny-bundle-fields-wrapper', $form['source_destiny_bundle_fields']));
        break;

      case 'paragraph':
        $bundle = $user_input['source_type_paragraph'];
        $form['destiny_type']['#value'] = 'paragraph';
        $form['source_destiny_bundle_fields']['source_type_paragraph']['#value'] = $bundle;
        $form['source_destiny_bundle_fields']['source_type']['#value'] = 'none';
        $form['source_destiny_bundle_fields']['source_type_vocabulary']['#value'] = 'none';
        $response->addCommand(new ReplaceCommand('.source-destiny-bundle-fields-wrapper', $form['source_destiny_bundle_fields']));
        break;
    }
    // To build the destiny_type key of each field in the form submit.
    $fields_array = $this->getDestinyFieldsByBundle($triggering_element['#attributes']['destiny_type'], $bundle);
    $form['source_destiny_fields_type'] = [
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#attributes' => ['style' => 'display: none;'],
      '#prefix' => '<div class="source-destiny-fields_type-wrapper" id="source-destiny-fields-type-wrapper">',
      '#suffix' => '</div>',
    ];
    foreach ($fields_array['fields_array_destiny_type'] as $key => $current_options_source_destiny_field_type) {
      $form['source_destiny_fields_type'][$key . '_type'] = [
        '#type' => 'hidden',
        '#name' => $key . '_type',
        '#value' => $current_options_source_destiny_field_type,
      ];
    }
    foreach ($this->fieldsOriginBundle as $field) {
      $form['source_destiny_fields'][$field['field_name']]['#options'] = $fields_array['fields_array'];
    }
    $form['source_destiny_fields']['workbench_access_node']['#options'] = $fields_array['fields_array'];
    $response->addCommand(new ReplaceCommand('.source-destiny-fields-wrapper', $form['source_destiny_fields']));
    $response->addCommand(new ReplaceCommand('.source-destiny-fields-type-wrapper', $form['source_destiny_fields_type']));
    $response->addCommand(new ReplaceCommand('.destiny_type_box', $form['destiny_type']));

    return $response;
  }

  /**
   * Get the fields media.
   *
   * Options for the media field selectors are generated
   * for the selected media and set to the corresponding.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   */
  public function getFieldsMedia(array &$form, FormStateInterface $form_state): AjaxResponse {
    $user_input = $form_state->getUserInput();
    $triggering_element = $form_state->getTriggeringElement();
    $response = new AjaxResponse();
    $fieldsMedia = $this->mwManageDataService->getFieldsMediaByBundle($user_input[$triggering_element['#name']]);
    $form['source_destiny_fields']['listmediafields/' . $triggering_element['#attributes']['type']]['#options'] = $fieldsMedia;
    $response->addCommand(new ReplaceCommand('.' . $triggering_element['#attributes']['destiny_ajax_callback'], $form['source_destiny_fields']['listmediafields/' . $triggering_element['#attributes']['type']]));

    return $response;
  }

  /**
   * Get the fields media destiny.
   *
   * Options for the media field selector are
   * generated in the media migration form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response
   *   @todo unify with the getFieldsMedia method
   */
  public function getFieldsMediaDestiny(array &$form, FormStateInterface $form_state): AjaxResponse {
    $user_input = $form_state->getUserInput();
    $triggering_element = $form_state->getTriggeringElement();
    $response = new AjaxResponse();
    $fieldsMedia = $this->mwManageDataService->getFieldsMediaByBundle($user_input[$triggering_element['#name']]);
    $form['source_destiny_fields']['listmediafields']['#options'] = $fieldsMedia;
    $response->addCommand(new ReplaceCommand('.' . $triggering_element['#attributes']['destiny_ajax_callback'], $form['source_destiny_fields']['listmediafields']));

    return $response;
  }

  /**
   * Get the fields origin bundle.
   *
   * Gets the fields of a source depending on its type.
   *
   * @param string $origin_entity
   *   The origin entity.
   * @param string $originType
   *   The origin type.
   *
   * @return array
   *   The fields origin bundle.
   */
  public function getFieldsOriginBundle($origin_entity, $originType): array {
    $fieldsOriginBundle = [];

    switch ($originType) {
      case 'node':
        $fieldsOriginBundle[] = [
          'field_name' => 'title',
          'type' => 'text',
          'data' => serialize(['label' => 'Title']),
          'fc_data' => serialize([]),
        ];
        break;

      case 'taxonomy_term':
        $fieldsOriginBundle[] = [
          'field_name' => 'name',
          'type' => 'text',
          'data' => serialize(['label' => 'name']),
          'fc_data' => serialize([]),
        ];
        $fieldsOriginBundle[] = [
          'field_name' => 'description',
          'type' => 'text_long',
          'data' => serialize(['label' => 'Description']),
          'fc_data' => serialize([]),
        ];
        break;

      case 'user':
        $fieldsOriginBundle[] = [
          'field_name' => 'picture',
          'type' => 'image',
          'data' => serialize(['label' => 'Picture']),
          'fc_data' => serialize([]),
        ];
        break;

      default:
        break;
    }

    return array_merge($fieldsOriginBundle, $this->mwManageDataService->getFieldsOriginBundle($origin_entity, $originType));
  }

  /**
   * Get the origin Content Types.
   *
   * Origin content type migrations availables
   * for selects on dependencies fields.
   *
   * @return array
   *   The options source origin type node.
   */
  public function getOriginContentTypes(): array {
    $options_source_origin_type_node = [];
    foreach ($this->mwManageDataService->entityTypeManager->getStorage('origin_node')->loadMultiple() as $key => $value) {
      $options_source_origin_type_node[$key] = $value->label();
    }
    return $options_source_origin_type_node;
  }

  /**
   * Get the destiny Vocabularies.
   *
   * @return array
   *   The options source destiny type vocabulary.
   */
  public function getOriginVocabularyTypes(): array {
    $options_source_origin_type_vocabulary = [];

    foreach ($this->mwManageDataService->entityTypeManager->getStorage('origin_vocabulary')->loadMultiple() as $key => $value) {
      $options_source_origin_type_vocabulary[$key] = $value->label();
    }

    return $options_source_origin_type_vocabulary;
  }

  /**
   * Set the config from form.
   *
   * @param array $config
   *   The config.
   */
  public function setConfig($config): void {
    $this->config = $config;
  }

  /**
   * Set the currentConfig from form.
   *
   * @param array $currentConfig
   *   The current config.
   */
  public function setCurrentConfig($currentConfig): void {
    $this->currentConfig = $currentConfig;
  }

  /**
   * Set the fieldsOriginBundle from form.
   *
   * @param array $fieldsOriginBundle
   *   The fields origin bundle.
   */
  public function setFieldsOriginBundle($fieldsOriginBundle): void {
    $this->fieldsOriginBundle = $fieldsOriginBundle;
  }

  /**
   * Submit the files form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function submitFilesForm(array &$form, FormStateInterface $form_state): void {
    $user_inputs = $form_state->getUserInput();
    $current_mw_database_shared_configuration = $this->mwDatabase->get('shared_configuration');
    $current_mw_database_shared_configuration['migration_files']['preserve_fid'] = $user_inputs['preserve_fid'];
    $current_mw_database_shared_configuration['migration_files']['build_migration_config'] = $user_inputs['build_migration_config'];
    $this->mwDatabase->set('shared_configuration', $current_mw_database_shared_configuration);
    $this->mwDatabase->save();

    if ($user_inputs['build_migration_config']) {
      $this->mwConfigBuilder->buildMigrationFilesConfig();
    }
  }

  /**
   * Submit the form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $user_inputs = $form_state->getUserInput();

    if (isset($user_inputs['source_type']) && ($user_inputs['source_type'] !== 'none')
      || (isset($user_inputs['source_type_vocabulary']) && $user_inputs['source_type_vocabulary'] !== 'none')
      || (isset($user_inputs['source_type_paragraph']) && $user_inputs['source_type_paragraph'] !== 'none')
      || (isset($user_inputs['source_type_media']) && $user_inputs['source_type_media'] !== 'none')) {
      $array_config = [];
      $bundle_type = '';
      $source = '';
      switch ($user_inputs['destiny_type']) {
        case 'node':
        case 'user':
          $source = 'source_type';
          break;

        case 'paragraph':
          $source = 'source_type_paragraph';
          break;

        case 'taxonomy_term':
          $source = 'source_type_vocabulary';
          break;

        case 'media':
          $source = 'source_type_media';
          break;
      }
      $bundle_type = $user_inputs[$source];
      $this->getDestinyFieldsByBundle($user_inputs['destiny_type'], $bundle_type);

      foreach ($this->fieldsOriginBundle as $field) {
        $field_name = $field['field_name'];
        $field_type = $field['type'];

        if (isset($user_inputs[$field_name])) {
          $destiny = $user_inputs[$field_name];
          $destiny_explode = explode('/', $destiny);
          $bundle = '';
          if (\count($destiny_explode) > 1) {
            $bundle = $destiny_explode[1];
          }
          $destiny_type = $user_inputs[$user_inputs[$field_name] . '_type'] ?? '';
          $target_bundles = [];
          if ($user_inputs[$field_name] !== 'none'
             && $this->entityFieldsDestiny[$user_inputs[$field_name]] !== NULL
             && $this->entityFieldsDestiny[$user_inputs[$field_name]]->getFieldStorageDefinition()->getSettings() === ['target_type' => 'paragraph']) {
            $target_bundles = $user_inputs[$field['field_name'] . '/target_bundle'];
            $destiny_type = 'paragraph';
            $paragraph_field_data = unserialize($field['data'], ['allowed_classes' => ['stdClass']]);
            $bundle = [];
            if (isset($paragraph_field_data['settings']['allowed_bundles'])) {
              foreach ($paragraph_field_data['settings']['allowed_bundles'] as $allowed_bundle) {
                if ($allowed_bundle !== -1) {
                  $bundle[] = $allowed_bundle;
                }
              }
            }
          }
          $array_config[$field_name] = [
            'type' => $this->arrayFieldsOriginBundle[$field_name],
            'destiny' => $user_inputs[$field_name],
            'destiny_type' => $destiny_type,
            'bundle' => $bundle ?? 'none',
            'target_bundles' => $target_bundles,
          ];
          if ($field_type === 'entityreference' || $field_type === 'taxonomy_term_reference' || $field_type === 'node_reference') {
            foreach ($form['source_destiny_fields'][$field_name]['#attributes']['dependencies'] as $key => $value) {
              $array_config[$field_name]['dependencies'][$key] = $user_inputs[$field_name . '/' . $key];
              $array_config[$field_name]['target_type'] = $form["source_destiny_fields"][$field_name]["#attributes"]["target_type"];
              $array_config[$field_name]['migration_lookup'][] = $user_inputs[$field_name . '/' . $key];
            }
          }
          if (($field_type === 'file' || $field_type === 'image') && isset($user_inputs['listmediatypes/' . $field_name], $user_inputs['listmediafields/' . $field_name])) {
            $array_config[$field_name]['media_type'] = $user_inputs['listmediatypes/' . $field_name];
            $array_config[$field_name]['media_field'] = $user_inputs['listmediafields/' . $field_name];
          }
        }
      }

      $origin_languages = $this->mwManageDataService->getOriginLanguages($user_inputs['origin_type'], $user_inputs['origin_bundle']);
      if (!isset($origin_languages['i18n_mode']) || $origin_languages['i18n_mode'] !== 2) {
        $array_config['languages']['default'] = $this->mwManageDataService->getOriginDefaultLangcode();
      }
      else {
        $array_config['languages']['default'] = $origin_languages['language'];
      }

      foreach ($user_inputs as $key => $value) {
        if ($value === 'none') {
          continue;
        }
        if (strpos($key, '/format') !== FALSE) {
          $key_sources = explode('/', $key);
          $array_config[$key_sources[0]]['format'] = $value;
        }
        if (strpos($key, '_lang') !== FALSE && $value == 1) {
          $langcode = str_replace('_lang', '', $key);
          $array_config['languages'][$langcode] = $langcode;
        }
      }

      if (isset($user_inputs['workbench_access_node']) && $user_inputs['workbench_access_node'] !== 'none') {
        $array_config['workbench_access_node']['destiny'] = $user_inputs['workbench_access_node'];
        $array_config['workbench_access_node']['type'] = 'workbench_access_node';
        $array_config['workbench_access_node']['dependencies']['workbench_access_node'] = $user_inputs['workbench_access_node/dependency/workbench_access_node'];
        $array_config['workbench_access_node']['migration_lookup'] = $user_inputs['workbench_access_node/dependency/workbench_access_node'];
      }
      $source_key = $user_inputs[$source];
      $array_config = [
        $source_key => $array_config,
      ];

      $array_config['build_migration_config'] = $user_inputs['build_migration_config'];

      if ($user_inputs['destiny_type'] !== 'paragraph') {
        if ($user_inputs['destiny_type'] !== 'user') {
          if ($user_inputs['origin_type'] === 'node' && isset($user_inputs['preserve_nid']) && $user_inputs['preserve_nid']) {
            $array_config['preserve_nid'] = $user_inputs['preserve_nid'];
          }
          elseif ($user_inputs['origin_type'] === 'taxonomy_term' && isset($user_inputs['preserve_tid']) && $user_inputs['preserve_tid']) {
            $array_config['preserve_tid'] = $user_inputs['preserve_tid'];
          }

          $array_config['import_users'] = $user_inputs['import_users'] ?? NULL;
          $array_config['source_type_date_field'] = $user_inputs['source_type_date_field'] ?? NULL;

          if (isset($user_inputs['i18n_mode'])) {
            $array_config['i18n_mode'] = $user_inputs['i18n_mode'];
          }
        }
        else {
          $array_config['preserve_uid'] = $user_inputs['preserve_uid'];

          if (isset($user_inputs['import_cas'])) {
            $array_config['import_cas'] = $user_inputs['import_cas'];
          }
        }
        $array_config['import_path_alias'] = $user_inputs['import_path_alias'];
        $array_config['limit_import'] = $user_inputs['limit_import'];
        $array_config['import_only_ids'] = $user_inputs['import_only_ids'];
      }
      $array_config['destiny_type'] = $user_inputs['destiny_type'];
      $array_config['origin_type'] = $user_inputs['origin_type'];
      $array_config['listmediafields'] = $user_inputs['listmediafields'] ?? NULL;

      $array_config['mw_database'] = $form['origin_database']['#value'];

      if ($form['origin_type']['#value'] !== 'user') {
        $origin_entity = $this->mwManageDataService->entityTypeManager->getStorage('origin_' . $form['origin_type']['#value'])->load($form['origin_bundle']['#value'] . '_' . $form['origin_database']['#value']);
        $array_config['mw_database'] = $form['origin_database']['#value'];
        $origin_entity->set('shared_configuration', $array_config)->save();
      }
      else {
        $origin_mw_database_entity = $this->mwManageDataService->entityTypeManager->getStorage('mw_database')->load($array_config['mw_database']);
        $shared_configuration = $origin_mw_database_entity->shared_configuration;
        $shared_configuration['user_migration_config'] = $array_config;
        $origin_mw_database_entity->set('shared_configuration', $shared_configuration)->save();
      }

      if ($user_inputs['build_migration_config'] && $user_inputs['destiny_type'] !== 'user') {
        $this->mwConfigBuilder->buildMigrationConfig($user_inputs['origin_bundle'], $array_config);
      }
      elseif ($user_inputs['build_migration_config']) {
        $this->mwConfigBuilder->buildMigrationUserConfig($user_inputs, $array_config);
      }
    }
  }

}
