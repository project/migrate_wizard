<?php

namespace Drupal\migrate_wizard\Form;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\migrate_wizard\MWManageDataService;
use Drupal\migrate_wizard\MWManageFormService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BaseFormFields.
 *
 * Provides Base for form fields.
 *
 * @package Drupal\migrate_wizard\Form
 *
 * @ingroup migrate_wizard
 */
abstract class BaseFormFields extends ConfigFormBase {

  /**
   * Array of fields from the origin bundle.
   *
   * @var array
   */
  protected $arrayFieldsOriginBundle;

  /**
   * Array of checkboxes for all forms.
   *
   * @var array
   */
  protected $checkboxes = [
    'build_migration_config' => 'Build migration config',
  ];

  /**
   * The current migration config.
   *
   * @var array
   */
  protected $currentConfig;

  /**
   * The destiny type.
   *
   * @var string
   */
  protected $destinyType;

  /**
   * The entity fields destiny.
   *
   * @var array
   */
  protected $entityFieldsDestiny;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The fields origin bundle.
   *
   * @var array
   */
  protected $fieldsOriginBundle;

  /**
   * The i18n mode (only for vocabularies)
   *
   * @var string
   */
  protected $i18nMode;

  /**
   * The lang codes.
   *
   * @var array
   */
  protected $langCodes = [
    'default_lang' => ['markup' => 'Default translation', 'value' => ''],
  ];

  /**
   * The manage form service.
   *
   * @var \Drupal\migrate_wizard\MWManageFormService
   */
  protected $manageFormService;

  /**
   * The mw manage data service.
   *
   * @var \Drupal\migrate_wizard\MWManageDataService
   */
  protected $mwManageDataService;

  /**
   * The origin bundle.
   *
   * @var string
   */
  protected $originBundle;

  /**
   * The origin type.
   *
   * @var string
   */
  protected $originType;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\migrate_wizard\MWManageDataService $mw_manage_data_service
   *   The service to manage data.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\migrate_wizard\MWManageFormService $manage_form_service
   *   The service to manage forms.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   */
  public function __construct(
      ConfigFactoryInterface $config_factory,
      MWManageDataService $mw_manage_data_service,
      EntityTypeManagerInterface $entity_type_manager,
      MWManageFormService $manage_form_service,
      protected $typedConfigManager = NULL) {

    parent::__construct($config_factory, $typedConfigManager);
    $this->mwManageDataService = $mw_manage_data_service;
    $this->entityTypeManager = $entity_type_manager;
    $this->destinyType = $this->currentConfig['destiny_type'] ?? '';
    $this->manageFormService = $manage_form_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('migrate_wizard.migrate_wizard_manage_data_service'),
      $container->get('entity_type.manager'),
      $container->get('migrate_wizard.migrate_wizard_manage_form_service'),
      $container->get('config.typed'),
    );
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param null $origin_entity
   *   The origin entity.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $origin_entity = NULL) {
    $form['origin_bundle'] = [
      '#type' => 'hidden',
      '#value' => $origin_entity->id_origin,
    ];

    if ($this->originType !== 'user' && $this->originType !== 'field_collection_item' && $this->originType !== 'paragraphs_item') {
      $this->destinyType = '';
      if ($this->currentConfig && isset($this->currentConfig['destiny_type'])) {
        $this->destinyType = $this->currentConfig['destiny_type'];
      }
    }
    $form['destiny_type'] = [
      '#type' => 'hidden',
      '#value' => $this->destinyType,
      '#prefix' => '<div class="destiny_type_box">',
      '#suffix' => '</div>',
    ];

    // Create checkbox options, in each case they can be different.
    $this->manageFormService->buildCheckboxes($form, $this->checkboxes);

    if ($this->currentConfig === NULL || $this->destinyType !== 'user') {
      $this->manageFormService->buildLangcodes($form, $this->langCodes, $this->i18nMode);
    }

    // Origin migrations availables for selects on dependencies fields.
    $options_source_origin_type_node = $this->manageFormService->getOriginContentTypes();
    $options_source_origin_type_vocabulary = $this->manageFormService->getOriginVocabularyTypes();
    $both_options_source = ['none' => 'Select dependency'] + [
      'Content types' => $options_source_origin_type_node,
      'Vocabularies' => $options_source_origin_type_vocabulary,
    ];

    $current_source_type_value = 'none';
    $current_source_type_vocabulary_value = 'none';
    $current_source_type_paragraph_value = 'none';

    // @todo investigate this, build ajaxcallback on service get error without this line
    $form_state->disableCache();

    // Build the source types select diferent in each case.
    // @todo switch here or refactor in one only method on service and switch inner
    if ($this->destinyType !== 'user' && $this->originType !== 'field_collection_item' && $this->originType !== 'paragraphs_item' && $this->originType !== 'media') {
      $this->manageFormService->buildSourceTypesForm($form, $current_source_type_value, $current_source_type_vocabulary_value, $current_source_type_paragraph_value);
    }
    elseif ($this->originType !== 'field_collection_item' && $this->originType !== 'paragraphs_item' && $this->originType !== 'media') {
      $form['source_type'] = [
        '#type' => 'hidden',
        '#value' => 'user',
      ];
    }
    elseif ($this->originType === 'field_collection_item' || $this->originType === 'paragraphs_item') {
      $this->manageFormService->buildSourceTypesParagraphsForm($form, $current_source_type_value);
    }
    elseif ($this->originType === 'media') {
      $this->manageFormService->buildSourceTypesMediasForm($form, $current_source_type_value);
    }

    // @todo refactor to switch or less code logic
    if ($this->originType !== 'field_collection_item') {
      $default_value = '';

      if (\is_array($this->currentConfig) && isset($this->currentConfig['import_only_ids'])) {
        $default_value = $this->currentConfig['import_only_ids'];
      }
      $form['import_only_ids'] = [
        '#type' => 'textfield',
        '#title' => $this->t('import only this ids'),
        '#description' => $this->t('write here the origin ids to import, separated by comma'),
        '#pattern' => '\d+(,\d+)*',
        '#size' => '20',
        '#default_value' => $default_value,
      ];

      $default_value = '';

      if (\is_array($this->currentConfig) && isset($this->currentConfig['limit_import'])) {
        $default_value = $this->currentConfig['limit_import'];
      }
      $form['limit_import'] = [
        '#type' => 'number',
        '#title' => $this->t('limit import'),
        '#description' => $this->t('write here the limit of items to import'),
        '#size' => '20',
        '#default_value' => $default_value,
      ];
    }

    if ($this->originType !== 'media') {
      $form['source_destiny_fields'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Fields'),
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
        '#prefix' => '<div class="source-destiny-fields-wrapper" id="source-destiny-fields-wrapper">',
        '#suffix' => '</div>',
      ];
    }
    $current_config_node = [];

    if (\is_array($this->currentConfig)) {
      $current_config_node = reset($this->currentConfig);
    }
    $generic_current_source_type_value = $current_source_type_value !== 'none' ? $current_source_type_value : $current_source_type_vocabulary_value;

    $current_options_source_destiny_fields_array['fields_array'] = ['none' => 'Select a field'];
    $current_options_source_destiny_fields_array['fields_array_destiny_type'] = [];

    $this->destinyType !== 'user' ?: $generic_current_source_type_value = $this->destinyType;

    if ($this->destinyType !== NULL && $generic_current_source_type_value !== 'none') {
      $current_options_source_destiny_fields_array = $this->manageFormService->getDestinyFieldsByBundle($this->destinyType, $generic_current_source_type_value);
    }

    if ($this->originType !== 'field_collection_item') {
      $this->manageFormService->buildWorkbenchAccessForm($form, $current_options_source_destiny_fields_array, $current_config_node, $both_options_source);
    }
    $form['source_destiny_fields_type'] = [
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#attributes' => ['style' => 'display: none;'],
      '#prefix' => '<div class="source-destiny-fields-type-wrapper" id="source-destiny-fields-type-wrapper">',
      '#suffix' => '</div>',
    ];

    foreach ($current_options_source_destiny_fields_array['fields_array_destiny_type'] as $key => $current_options_source_destiny_field_type) {
      $form['source_destiny_fields_type'][$key . '_type'] = [
        '#type' => 'hidden',
        '#name' => $key . '_type',
        '#value' => $current_options_source_destiny_field_type,
      ];
    }

    $this->manageFormService->buildFieldsForm($form, $current_options_source_destiny_fields_array['fields_array'], $generic_current_source_type_value, $both_options_source, $current_config_node, $this->fieldsOriginBundle);

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];
    $form['#attached']['library'][] = 'migrate_wizard/form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle($type = NULL): TranslatableMarkup {
    $current_path = $this->manageFormService->pathCurrent->getPath();
    $path = explode('/', $current_path);
    $path = end($path);
    $bundle = str_replace('_' . $this->mwManageDataService->getMwDatabase()->id, '', $path);
    return $this->t('Manage fields @type', ['@type' => $this->mwManageDataService->getOriginBundleTitle($bundle, $type)]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->manageFormService->submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['migrate_wizard.settings'];
  }

}
