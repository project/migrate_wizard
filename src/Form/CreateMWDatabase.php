<?php

namespace Drupal\migrate_wizard\Form;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CreateMWDatabase.
 *
 * Provides form to create a new mw_database.
 *
 * @package Drupal\migrate_wizard\Form
 *
 * @ingroup migrate_wizard
 */
class CreateMWDatabase extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected $migrateWizardSettings;

  /**
   * {@inheritdoc}
   */
  protected $entityTypeManager;

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $configFactory, EntityTypeManagerInterface $entity_type_manager) {
    $this->migrateWizardSettings = $configFactory->getEditable('migrate_wizard.settings');
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $database_fields = [
      'id' =>$this->t('Migration ID'),
      'migration_group' => $this->t('Migration group'),
      'key' => $this->t('Key'),
      'target' => $this->t('Target'),
      'host' => $this->t('Database host'),
      'database' => $this->t('Database name'),
      'username' => $this->t('Database username'),
      'password' => $this->t('Database password'),
      'prefix' => $this->t('Database prefix'),
      'driver' => $this->t('Database driver'),
      'port' => $this->t('Database port'),
      'namespace' => $this->t('Database driver namespace'),
      'collation' => $this->t('Database collation'),
      'url_files_prod' => $this->t('Production URL'),
    ];
    $use_envs = ['key', 'target', 'host', 'database', 'username', 'password'];
    $form['use_env'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use .env *'),
    ];
    foreach ($database_fields as $key => $database_field) {
      $title = $database_field;
      if (in_array($key, $use_envs)) {
        $title = $title . ' *';
      }
      $form[$key] = [
        '#type' => 'textfield',
        '#title' => $title,
        '#size' => '20',
      ];
    }
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'create_mw_database';
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(): TranslatableMarkup {
    return $this->t('Create MW Database');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $user_inputs = $form_state->getUserInput();
    $migrate_wizard_settings = $this->migrateWizardSettings->get('migrate_wizard_settings');

    if (!isset($migrate_wizard_settings['databases']) || !isset($migrate_wizard_settings['databases'][$user_inputs['id']])) {
      $this->entityTypeManager->getStorage('mw_database')->create($user_inputs)->save();
      $migrate_wizard_settings['databases'][$user_inputs['id']] = [
        'host' => $user_inputs['host'],
        'database' => $user_inputs['database'],
        'username' => $user_inputs['username'],
        'password' => $user_inputs['password'],
      ];
      $this->migrateWizardSettings->set('migrate_wizard_settings', $migrate_wizard_settings)->save();
      // Clear caches.
      drupal_flush_all_caches();
      $form_state->setRedirect('entity.database.list');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $user_inputs = $form_state->getUserInput();
    $migrate_wizard_settings = $this->migrateWizardSettings->get('migrate_wizard_settings');

    if (isset($migrate_wizard_settings['databases'], $migrate_wizard_settings['databases'][$user_inputs['id']])) {
      $form_state->setErrorByName('id', $this->t('Database configuration already exists'));
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['migrate_wizard.settings'];
  }

}
