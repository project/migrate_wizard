<?php

namespace Drupal\migrate_wizard\Form;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class OriginFieldCollectionItemFields.
 *
 * Provides form create and edit migration field collections.
 *
 * @package Drupal\migrate_wizard\Form
 *
 * @ingroup migrate_wizard
 */
class OriginFieldCollectionItemFields extends BaseFormFields {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $origin_bundle = NULL) {
    $this->originBundle = $origin_bundle;
    $this->originType = 'field_collection_item';
    $origin_field_collection_entity = $this->entityTypeManager->getStorage('origin_field_collection_item')->load($this->originBundle);
    $this->currentConfig = $origin_field_collection_entity->shared_configuration;
    $this->manageFormService->setCurrentConfig($origin_field_collection_entity->shared_configuration);
    $this->fieldsOriginBundle = $this->manageFormService->getFieldsOriginBundle($origin_field_collection_entity, $this->originType);
    $this->manageFormService->setFieldsOriginBundle($this->fieldsOriginBundle);
    $this->destinyType = 'paragraph';

    $form['origin_type'] = [
      '#type' => 'hidden',
      '#value' => 'field_collection_item',
    ];

    $form['origin_bundle'] = [
      '#type' => 'hidden',
      '#value' => $origin_field_collection_entity->id_origin,
    ];

    $origin_database_value = $origin_field_collection_entity->database;
    $form['origin_database'] = [
      '#type' => 'hidden',
      '#value' => $origin_database_value,
    ];

    $this->langCodes['default_lang']['value'] = $this->mwManageDataService->getOriginDefaultLangcode();

    $config_translation = $this->mwManageDataService->getOriginLanguages($form['origin_type']['#value'], $origin_bundle);

    foreach ($config_translation['origin_languages'] as $language) {
      $this->langCodes[$language . '_lang'] = [
        'markup' => $language,
        'value' => $language,
      ];
    }

    return parent::buildForm($form, $form_state, $origin_field_collection_entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'manage_origin_field_collection_fields';
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle($type = NULL): TranslatableMarkup {
    $current_path = $this->manageFormService->pathCurrent->getPath();
    $path = explode('/', $current_path);
    $path = end($path);

    return $this->t('Manage fields @path', ['@path' => $path]);
  }

}
