<?php

namespace Drupal\migrate_wizard\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Class MWMigrationFiles.
 *
 * Provides form create and edit migration files.
 *
 * @package Drupal\migrate_wizard\Form
 *
 * @ingroup migrate_wizard
 */
class MWMigrationFiles extends BaseFormFields {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $origin_bundle = NULL) {
    $this->originType = 'files';
    if (isset($this->mwManageDataService->getMwDatabase()->shared_configuration['migration_files'])) {
      $this->currentConfig = $this->mwManageDataService->getMwDatabase()->shared_configuration['migration_files'];
      $this->manageFormService->setCurrentConfig($this->mwManageDataService->getMwDatabase()->shared_configuration['migration_files']);
    }
    $type_fields_checkboxes = [
      'preserve_fid' => $this->t('Preserve fid'),
    ];
    $this->checkboxes = array_merge($this->checkboxes, $type_fields_checkboxes);
    $this->manageFormService->buildCheckboxes($form, $this->checkboxes);
    $form['origin_type'] = [
      '#type' => 'hidden',
      '#value' => 'file',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mw_migration_files';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->manageFormService->submitFilesForm($form, $form_state);
  }

}
