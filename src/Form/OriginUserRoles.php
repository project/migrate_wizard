<?php

namespace Drupal\migrate_wizard\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\migrate_wizard\MWManageDataService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class OriginUserRoles.
 *
 * Provides form create and edit migration user's roles.
 *
 * @package Drupal\migrate_wizard\Form
 *
 * @ingroup migrate_wizard
 */
class OriginUserRoles extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected $mwManageDataService;

  /**
   * {@inheritdoc}
   */
  protected $roles;

  /**
   * {@inheritdoc}
   */
  public function __construct(MWManageDataService $mw_manage_data_service, EntityTypeManagerInterface $entity_type_manager) {
    $this->mwManageDataService = $mw_manage_data_service;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('migrate_wizard.migrate_wizard_manage_data_service'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $mw_database = $this->mwManageDataService->getMwDatabase();
    $shared_configuration = $mw_database->get('shared_configuration');
    $current_config = $shared_configuration['user_roles'] ?? [];
    $this->roles = $this->mwManageDataService->getOriginRoles();
    $current_roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    $current_roles_array = [
      'none' => $this->t('Do not migrate'),
      'create' => $this->t('Create Rol'),
    ];

    foreach ($current_roles as $current_role) {
      $current_roles_array[$current_role->id()] = $current_role->label();
    }

    foreach ($this->roles as $key_origin_role => $origin_role) {
      $current_value = 'none';

      if ($current_config && isset($current_config['roles'][$key_origin_role]['destiny'])) {
        $current_value = $current_config['roles'][$key_origin_role]['destiny'];
      }
      $form['roles'][$key_origin_role] = [
        '#type' => 'select',
        '#title' => $origin_role,
        '#options' => $current_roles_array,
        '#validated' => TRUE,
        '#value' => $current_value,
      ];
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'manage_origin_node_fields';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $user_inputs = $form_state->getUserInput();
    $array_config = [];

    foreach ($this->roles as $key_rol => $rol) {
      $array_config['roles'][$key_rol] = [
        'origin' => $this->roles[$key_rol],
        'destiny' => $user_inputs[$key_rol],
      ];
    }

    $this->mwManageDataService->createRoles($array_config);

    $mw_database = $this->mwManageDataService->getMwDatabase();
    $shared_configuration = $mw_database->get('shared_configuration');
    $shared_configuration['user_roles'] = $array_config;
    $mw_database->set('shared_configuration', $shared_configuration)->save();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['migrate_wizard.settings'];
  }

}
