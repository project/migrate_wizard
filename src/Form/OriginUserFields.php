<?php

namespace Drupal\migrate_wizard\Form;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class OriginUserFields.
 *
 * Provides form create and edit migration users.
 *
 * @package Drupal\migrate_wizard\Form
 *
 * @ingroup migrate_wizard
 */
class OriginUserFields extends BaseFormFields {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $origin_bundle = NULL) {
    $this->originType = 'user';
    $this->destinyType = 'user';

    if (isset($this->mwManageDataService->getMwDatabase()->shared_configuration['user_migration_config'])) {
      $this->currentConfig = $this->mwManageDataService->getMwDatabase()->shared_configuration['user_migration_config'];
      $this->manageFormService->setCurrentConfig($this->mwManageDataService->getMwDatabase()->shared_configuration['user_migration_config']);
    }

    $origin_entity = new \stdClass();
    $origin_entity->id_origin = 'user';
    $origin_entity->key = $this->mwManageDataService->getMwDatabase()->key;
    $origin_entity->target = $this->mwManageDataService->getMwDatabase()->target;

    $origin_database_value = $this->mwManageDataService->getMwDatabase()->id;
    $form['origin_database'] = [
      '#type' => 'hidden',
      '#value' => $origin_database_value,
    ];

    $this->fieldsOriginBundle = $this->manageFormService->getFieldsOriginBundle($origin_entity, $this->originType);
    $this->manageFormService->setFieldsOriginBundle($this->fieldsOriginBundle);
    $type_fields_checkboxes = [
      'preserve_uid' => $this->t('Preserve uid'),
      'import_path_alias' => $this->t('Import path alias'),
    ];

    if ($this->mwManageDataService->checkIfCasModuleOriginExistsEnabled() && $this->manageFormService->moduleHandler->moduleExists('media')) {
      $type_fields_checkboxes['import_cas'] = $this->t('Import cas');
    }

    $this->checkboxes = array_merge($this->checkboxes, $type_fields_checkboxes);
    $form['origin_type'] = [
      '#type' => 'hidden',
      '#value' => 'user',
    ];

    $this->langCodes['default_lang']['value'] = $this->mwManageDataService->getOriginDefaultLangcode();

    $config_translation = $this->mwManageDataService->getOriginLanguages($form['origin_type']['#value'], $origin_bundle);

    foreach ($config_translation['origin_languages'] as $language) {
      $this->langCodes[$language . '_lang'] = [
        'markup' => $language,
        'value' => $language,
      ];
    }

    return parent::buildForm($form, $form_state, $origin_entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'manage_origin_user_fields';
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle($type = NULL): TranslatableMarkup {
    return $this->t('Manage fields User');
  }

}
