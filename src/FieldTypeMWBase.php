<?php

namespace Drupal\migrate_wizard;

/**
 * Base class for FieldType plugins.
 */
abstract class FieldTypeMWBase {

  /**
   * Get the MWDatabase entity.
   *
   * @param string $mwDatabase
   *   The MWDatabase entity id.
   *
   * @return mixed
   *   The MWDatabase entity.
   */
  public static function getMwDatabase($mwDatabase) {
    return \Drupal::service('entity_type.manager')->getStorage('mw_database')->load($mwDatabase);
  }

  /**
   * Insert a value before a key in an array.
   *
   * @param array $originalArray
   *   The original array.
   * @param string $originalKey
   *   The original key.
   * @param string $insertKey
   *   The key to insert.
   * @param mixed $insertValue
   *   The value to insert.
   */
  public static function insertBeforeKey($originalArray, $originalKey, $insertKey, $insertValue) {
    $newArray = [];
    $inserted = FALSE;

    foreach ($originalArray as $key => $value) {
      if (!$inserted && $key === $originalKey) {
        if (!isset($newArray[$insertKey])) {
          $newArray[$insertKey] = [$insertValue];
        }
        else {
          $newArray[$insertKey][] = $insertValue;
        }
        $inserted = TRUE;
      }
      $newArray[$key] = $value;
    }

    return $newArray;
  }

}
