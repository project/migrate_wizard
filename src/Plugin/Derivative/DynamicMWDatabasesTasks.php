<?php

namespace Drupal\migrate_wizard\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DynamicMWDatabasesTasks.
 *
 * Provides dynamic task for migrate wizard settings menu.
 *
 * @package Drupal\migrate_wizard\Plugin\Derivative
 *
 * @ingroup migrate_wizard
 */
class DynamicMWDatabasesTasks extends DeriverBase implements ContainerDeriverInterface {

  /**
   * EntityTypeManager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  private $entityTypeManager;

  /**
   * Constructs a new DynamicMWDatabasesTasks object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $mw_databases = $this->entityTypeManager->getStorage('mw_database')->loadMultiple();

    foreach ($mw_databases as $mw_database) {
      $this->derivatives['mw_database.' . $mw_database->id] = $base_plugin_definition;
      $this->derivatives['mw_database.' . $mw_database->id]['base_route'] = 'entity.database.list';
      $this->derivatives['mw_database.' . $mw_database->id]['title'] = $mw_database->id;
      $this->derivatives['mw_database.' . $mw_database->id]['route_name'] = 'entity.origin_users_roles.settings';
      $this->derivatives['mw_database.' . $mw_database->id]['route_parameters'] = ['mw_database' => $mw_database->id];

      $links = [
        'user_roles' => [
          'route_name' => 'entity.origin_users_roles.settings',
          'route_parameters' => ['mw_database' => $mw_database->id],
          'title' => 'Users Roles',
        ],
        'users' => [
          'route_name' => 'entity.origin_users.settings',
          'route_parameters' => [
            'mw_database' => $mw_database->id,
            'origin_bundle' => 'user',
          ],
          'title' => 'Users',
        ],
        'mapping_format_editors' => [
          'route_name' => 'migrate_wizard.mapping',
          'route_parameters' => ['mw_database' => $mw_database->id],
          'title' => 'Mapping format editors',
        ],
        'node_list' => [
          'route_name' => 'entity.origin_node.list',
          'route_parameters' => ['mw_database' => $mw_database->id],
          'title' => 'Node list',
        ],
        'vocabularies_list' => [
          'route_name' => 'entity.origin_vocabularies.list',
          'route_parameters' => ['mw_database' => $mw_database->id],
          'title' => 'Vocabularies list',
        ],
        'field_collection_list' => [
          'route_name' => 'entity.origin_field_collection.list',
          'route_parameters' => ['mw_database' => $mw_database->id],
          'title' => 'Manage Origin Field Collections',
        ],
        'paragraph_list' => [
          'route_name' => 'entity.origin_paragraph.list',
          'route_parameters' => ['mw_database' => $mw_database->id],
          'title' => 'Manage Origin Paragraphs',
        ],
        'migration_files' => [
          'route_name' => 'migrate_wizard.migrate_files.configuration',
          'route_parameters' => ['mw_database' => $mw_database->id],
          'title' => 'Manage Migration files',
        ],
        'migration_medias' => [
          'route_name' => 'entity.origin_medias.list',
          'route_parameters' => ['mw_database' => $mw_database->id],
          'title' => 'Manage Migration Medias',
        ],
      ];

      foreach ($links as $link_key => $link) {
        $this->derivatives[$link_key . '.' . $mw_database->id] = $base_plugin_definition;
        $this->derivatives[$link_key . '.' . $mw_database->id]['route_name'] = $link['route_name'];
        $this->derivatives[$link_key . '.' . $mw_database->id]['route_parameters'] = $link['route_parameters'];
        $this->derivatives[$link_key . '.' . $mw_database->id]['title'] = $link['title'];
        $this->derivatives[$link_key . '.' . $mw_database->id]['parent_id'] = 'migrate_wizard.current_databases:mw_database.' . $mw_database->id;
      }
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
