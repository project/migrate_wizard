<?php

namespace Drupal\migrate_wizard\Plugin\Derivative;

use Drupal\migrate\Plugin\Derivative\MigrateEntityRevision;
use Drupal\migrate_wizard\Plugin\migrate\destination\MWEntityReferenceRevisions;

/**
 * Class MWMigrateEntityReferenceRevisions.
 *
 * Provides entity_reference_revisions destination plugin.
 *
 * @MigrateDestination(
 *   id="mw_entity_reference_revisions",
 *   deriver="Drupal\migrate_wizard\Plugin\Derivative\MWMigrateEntityReferenceRevisions"
 * )
 * @package Drupal\migrate_wizard\Plugin\Derivative
 * @see \Drupal\migrate_wizard\Plugin\migrate\destination\MWEntityReferenceRevisions
 */
class MWMigrateEntityReferenceRevisions extends MigrateEntityRevision {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($basePluginDefinition) {
    foreach ($this->entityDefinitions as $entityType => $entityInfo) {
      if ($entityInfo->getKey('revision')) {
        $this->derivatives[$entityType] = [
          'id' => "entity_reference_revisions:{$entityType}",
          'class' => MWEntityReferenceRevisions::class,
          'requirements_met' => 1,
          'provider' => $entityInfo->getProvider(),
        ];
      }
    }

    return $this->derivatives;
  }

}
