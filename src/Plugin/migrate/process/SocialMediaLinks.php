<?php

namespace Drupal\migrate_wizard\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\link\Plugin\migrate\process\FieldLink;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\social_media_links\SocialMediaLinksPlatformManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Social media links.
 *
 * @MigrateProcessPlugin(
 *     id="social_media_links"
 * )
 */
class SocialMediaLinks extends FieldLink implements ContainerFactoryPluginInterface {

  /**
   * The social link field plugin manager.
   *
   * @var \Drupal\social_media_links\SocialMediaLinksPlatformManager
   */
  private $socialLinkFieldPluginManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, ?SocialMediaLinksPlatformManager $social_link_field_plugin_manager = NULL) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    $this->socialLinkFieldPluginManager = $social_link_field_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, ?MigrationInterface $migration = NULL) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->has('plugin.manager.social_link_field.platform') ? $container->get('plugin.manager.social_link_field.platform') : NULL,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $plugins = $this->socialLinkFieldPluginManager->getPlatforms();

    $url = parse_url($value['url'], \PHP_URL_SCHEME) . '://' . parse_url($value['url'], \PHP_URL_HOST);

    $array_values = ['link' => str_replace($url, '', $value['url'])];

    foreach ($plugins as $key_plugin => $plugin) {
      if (strpos($value['url'], $key_plugin)) {
        $array_values['social'] = $key_plugin;

        break;
      }
    }

    return $array_values;
  }

}
