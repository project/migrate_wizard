<?php

namespace Drupal\migrate_wizard\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\migrate_wizard\Plugin\migrate\destination\MWEntityFile;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Geofield explode.
 *
 * @MigrateProcessPlugin(
 *     id="mw_process_entity_file"
 * )
 */
class MWProcessEntityFile extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityFilePlugin;

  /**
   * Constructs a new MWProcessEntityFile object.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \Drupal\migrate_wizard\Plugin\migrate\destination\MWEntityFile $entity_file_plugin
   *   The entity file plugin.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, MWEntityFile $entity_file_plugin) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityFilePlugin = $entity_file_plugin;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, ?MigrationInterface $migration = NULL) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.migrate.destination')->createInstance('mwentity:file', $configuration, $migration)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $destination = $row->getSourceProperty('uri');

    if (empty($destination)) {
      throw new MigrateException('Destination property uri not provided');
    }
    $entity = $this->entityFilePlugin->getEntity($row, []);
    $entity->save();

    return $entity->id();
  }

}
