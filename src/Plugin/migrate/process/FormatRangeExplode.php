<?php

namespace Drupal\migrate_wizard\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Format range explode.
 *
 * @MigrateProcessPlugin(
 *     id="format_range_explode"
 * )
 */
class FormatRangeExplode extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $values = explode('||', $value[0]);
    $values2 = explode('||', $value[1]);
    $new_value = [];

    foreach ($values as $key => $value) {
      $new_value[] = [
        'from' => $value,
        'to' => $values2[$key],
      ];
    }

    return $new_value;
  }

}
