<?php

namespace Drupal\migrate_wizard\Plugin\migrate\process;

use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Concatenates a set of strings.
 *
 * @MigrateProcessPlugin(
 *     id="mw_concat",
 *     handle_multiples=TRUE
 * )
 */
class MWConcat extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (\is_array($value)) {
      $delimiter = $this->configuration['delimiter'] ?? '';

      return implode($delimiter, $value);
    }

    throw new MigrateException(\sprintf('%s is not an array', var_export($value, TRUE)));
  }

}
