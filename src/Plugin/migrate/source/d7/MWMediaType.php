<?php

namespace Drupal\migrate_wizard\Plugin\migrate\source\d7;

use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 7 Filemime types from database to separate media types migration.
 *
 * For available configuration keys, refer to the parent classes.
 *
 * @see \Drupal\migrate\Plugin\migrate\source\SqlBase
 * @see \Drupal\migrate\Plugin\migrate\source\SourcePluginBase
 *
 * @MigrateSource(
 *     id="mw_d7_media_type",
 *     source_module="migrate_wizard"
 * )
 */
class MWMediaType extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['filemime']['filemime'] = 'string';
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('file_managed', 'fm')->fields('fm', ['filemime']);
    $query->groupBy('filemime');
    return $query;
  }

}
