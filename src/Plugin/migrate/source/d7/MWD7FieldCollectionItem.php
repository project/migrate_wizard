<?php

namespace Drupal\migrate_wizard\Plugin\migrate\source\d7;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate_wizard\FieldTypeMWManager;
use Drupal\paragraphs\Plugin\migrate\source\d7\FieldCollectionItem;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Language\Language;

/**
 * Field Collection Item Translation source plugin.
 *
 * Available configuration keys:
 * - field_name: (required) This will only return field collections of that
 * particular type.
 *
 * @MigrateSource(
 *     id="mw_d7_field_collection_item",
 *     source_module="field_collection",
 * )
 */
class MWD7FieldCollectionItem extends FieldCollectionItem {

  /**
   * Array of bundles.
   *
   * @var array
   */
  public $bundle;

  /**
   * Query Sql to add conditions.
   *
   * @var Query
   */
  public $query;

  /**
   * The current config of content to import.
   *
   * @var array
   */
  protected $currentConfig;

  /**
   * The default langcode.
   *
   * @var string
   */
  protected $defaultLangcode;

  /**
   * Array of nid or vid destiny type.
   *
   * @var string
   */
  protected $destinyType;

  /**
   * The entity type.
   *
   * @var string
   */
  protected $entityType;

  /**
   * The field type manager.
   *
   * @var \Drupal\migrate_wizard\FieldTypeMWManager
   */
  protected $fieldTypeMWManager;

  /**
   * The left join table.
   *
   * @var string
   */
  protected $leftJoinTable;

  /**
   * The left table.
   *
   * @var string
   */
  protected $leftTable;

  /**
   * The left join table id.
   *
   * @var string
   */
  protected $leftTableId;

  /**
   * The current mw_database.
   *
   * @var \Drupal\migrate_wizard\Entity\MWDatabase
   */
  protected $mwDatabase;

  /**
   * Array of machine name of origin to import.
   *
   * @var string
   */
  protected $originType;

  /**
   * The available translations.
   *
   * @var array
   */
  protected $translation;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    MigrationInterface $migration,
    StateInterface $state,
    EntityTypeManagerInterface $entity_type_manager,
    FieldTypeMWManager $field_type_mw_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_type_manager);

    $this->state = $state;
    $current_source_config = $migration->getSourceConfiguration();

    $this->bundle = $current_source_config['field_name'];

    if (isset($current_source_config['constants']['translations']) && $current_source_config['constants']['translations']) {
      $this->bundle = str_replace('_' . $current_source_config['constants']['langcode'], '', $current_source_config['bundle']);
    }
    $this->mwDatabase = $this->entityTypeManager->getStorage('mw_database')->load($configuration['mw_database']);
    $current_entity = $this->entityTypeManager->getStorage('origin_field_collection_item')->load($this->bundle . '_' . $configuration['mw_database']);
    $this->currentConfig = $current_entity->get('shared_configuration');

    $this->defaultLangcode = $configuration['constants']['default_langcode'];

    $this->translation = [Language::LANGCODE_NOT_SPECIFIED];

    if (isset($this->currentConfig[array_key_first($this->currentConfig)]['languages'])) {
      $this->translation = [
        Language::LANGCODE_NOT_SPECIFIED,
        $this->currentConfig[array_key_first($this->currentConfig)]['languages']['default'],
      ];
    }

    if (isset($configuration['constants']['translations']) && $configuration['constants']['translations']) {
      $this->translation = [$configuration['constants']['langcode']];
    }
    $this->fieldTypeMWManager = $field_type_mw_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, ?MigrationInterface $migration = NULL) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.field_type_mw')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    $query->addField('fc', 'delta', 'delta_field_collection');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $prepare_row_result = parent::prepareRow($row);
    if (isset($this->configuration['constants']['translations']) && $this->configuration['constants']['translations']) {
      $this->getIdTranslationLookup(
        $row,
        $row->getSourceProperty('parent_type'),
        $this->configuration['field_name'],
        $row->getSourceProperty('item_id')
      );
    }
    $language = $this->getLangcode($row->getSourceProperty('parent_type'), $row->getSourceProperty('field_name'), $row->getSourceProperty('item_id'));
    if ($language) {
      $row->setSourceProperty('language', $language);
    }
    foreach (array_keys($this->getFields($this->currentConfig['origin_type'], $this->bundle)) as $field) {
      if (isset(reset($this->currentConfig)[$field]) && reset($this->currentConfig)[$field]['type'] === 'field_collection') {
        $new_field_source = [];
        $field_source = $row->getSourceProperty($field);

        foreach ($field_source as $field_source_key => $field_source_value) {
          $new_field_source[$field_source_key] = [
            'item_id' => $field_source_value['value'],
            'revision_id' => $field_source_value['revision_id'],
          ];
        }
        $row->setSourceProperty($field, $new_field_source);
      }
    }
    return $prepare_row_result;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'item_id' => $this->t('The field_collection_item id'),
      'revision_id' => $this->t('The field_collection_item revision id'),
      'bundle' => $this->t('The field_collection bundle'),
      'field_name' => $this->t('The field_collection field_name'),
      'parent_type' => $this->t('The type of the parent entity'),
      'parent_id' => $this->t('The identifier of the parent entity'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'item_id' => [
        'type' => 'integer',
        'alias' => 'f',
      ],
      'revision_id' => [
        'type' => 'integer',
        'alias' => 'f',
      ],
    ];
  }

  /**
   * If is a translation, it will lookup the id.
   */
  private function getIdTranslationLookup($row, $parent_type, $field_name, $item_id, &$array_tree = NULL): void {
    if ($parent_type === 'field_collection_item') {
      if (!$array_tree) {
        $array_tree[] = [
          'entity_type' => 'field_collection_item',
          'bundle' => $field_name,
          'entity_id' => $item_id,
          'delta' => $row->getSourceProperty('delta_field_collection'),
        ];
      }
      $query = $this->select('field_data_' . $field_name, 'fdc')
        ->fields('fdc', ['entity_type', 'bundle', 'entity_id', 'delta']);
      $query->condition('fdc.' . $field_name . '_value', $item_id);
      $result = $query->execute()->fetchAssoc();
      $array_tree[] = $result;

      if ($result['entity_type'] === 'field_collection_item') {
        $this->getIdTranslationLookup($row, $result['entity_type'], $result['bundle'], $result['entity_id'], $array_tree);
      }
      else {
        $query = $this->select('field_data_' . $array_tree[0]['bundle'], 'fdc')
          ->fields('fdc', [$array_tree[0]['bundle'] . '_value'])
          ->condition('fdc.delta', $array_tree[0]['delta']);

        foreach ($array_tree as $key => $join_tree) {
          if ($key === 0) {
            continue;
          }
          if ($join_tree['entity_type'] === 'field_collection_item') {
            $suffix = $key - 1;

            if ($suffix === 0) {
              $suffix = '';
            }
            $query->leftJoin('field_data_' . $join_tree['bundle'], 'fdc' . $key, "fdc{$key}.{$join_tree['bundle']}_value = fdc{$suffix}.{$array_tree[$key - 1]['bundle']}_value");
          }
          elseif ($join_tree['entity_type'] === 'node') {
            $query_tnid = $this->select('node', 'n');
            $query_tnid->condition('n.nid', $join_tree['entity_id']);
            $query_tnid->leftJoin('node', 'n2', 'n2.nid = n.tnid AND n2.nid!= n.nid');
            $query_tnid->addField('n2', 'nid', 'n2_nid');
            $nid = $query_tnid->execute()->fetchField();
            if ($nid) {
              $query->leftJoin('node', 'n', "n.nid = {$nid}");
              $id_lookup = $query->execute()->fetchField();
              $row->setSourceProperty('id_lookup', $id_lookup);
            }
          }
        }
      }
    }
    elseif ($parent_type === 'node') {
      $query_tnid = $this->select('node', 'n');
      $query_tnid->condition('n.nid', $row->getSourceProperty('parent_id'));
      $query_tnid->leftJoin('node', 'n2', 'n2.nid = n.tnid AND n2.nid!= n.nid');
      $query_tnid->leftJoin('field_data_' . $row->getSourceProperty('field_name'), 'fdf', 'fdf.entity_id = n2.nid AND fdf.delta = ' . $row->getSourceProperty('delta_field_collection'));
      $query_tnid->addField('fdf', $row->getSourceProperty('field_name') . '_value', 'fsf_val');
      $nid = $query_tnid->execute()->fetchField();
      if ($nid) {
        $row->setSourceProperty('id_lookup', $nid);
      }
    }
  }

  /**
   * Get the langcode of the node that contains the field collection in origin.
   */
  private function getLangcode($parent_type, $field_name, $item_id, $depth = 0) {
    if ($parent_type === 'field_collection_item' || $depth === 0) {
      $query = $this->select('field_data_' . $field_name, 'fdc')
        ->fields('fdc', ['entity_type', 'bundle', 'entity_id']);
      $query->condition('fdc.' . $field_name . '_value', $item_id);
      $result = $query->execute()->fetchAssoc();
      return $this->getLangcode($result['entity_type'], $result['bundle'], $result['entity_id'], $depth + 1);
    }
    $query = $this->select('node', 'n');
    $query->condition('nid', $item_id);
    $query->addField('n', 'language', 'language');
    if (isset($this->configuration['constants']['translations']) && $this->configuration['constants']['translations']) {
      $query->condition('n.language', $this->configuration['constants']['langcode']);
    }
    else {
      $query->condition('n.language', [
        Language::LANGCODE_NOT_SPECIFIED,
        $this->configuration['constants']['default_langcode'],
      ], 'in');
    }
    return $query->execute()->fetchField();
  }

}
