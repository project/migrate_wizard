<?php

namespace Drupal\migrate_wizard\Plugin\migrate\source\d7;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate_wizard\FieldTypeMWManager;
use Drupal\taxonomy\Plugin\migrate\source\d7\Term;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Drupal 7 taxonomy term source from database.
 *
 * @MigrateSource(
 *     id="mw_d7_taxonomy_term",
 *     source_module="taxonomy"
 * )
 */
class MWD7Term extends Term {

  /**
   * Array of bundles.
   *
   * @var array
   */
  public $bundle;

  /**
   * Query Sql to add conditions.
   *
   * @var Query
   */
  public $query;

  /**
   * The config of content type to import.
   *
   * @var string
   */
  protected $config;

  /**
   * The config of content type to import.
   *
   * @var string
   */
  protected $configFactory;

  /**
   * The current config of content to import.
   *
   * @var array
   */
  protected $currentConfig;

  /**
   * Array of nid or vid destiny type.
   *
   * @var string
   */
  protected $destinyType;

  /**
   * The entity type.
   *
   * @var string
   */
  protected $entityType;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $fieldTypeMWManager;

  /**
   * The left join table.
   *
   * @var string
   */
  protected $leftJoinTable;

  /**
   * The left table.
   *
   * @var string
   */
  protected $leftTable;

  /**
   * The left table id.
   *
   * @var string
   */
  protected $leftTableId;

  /**
   * The current mw_database.
   *
   * @var \Drupal\migrate_wizard\Entity\MWDatabase
   */
  protected $mwDatabase;

  /**
   * Array of machine name of origin to import.
   *
   * @var string
   */
  protected $originType;

  /**
   * Array of translation.
   *
   * @var array
   */
  protected $translation;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    MigrationInterface $migration,
    StateInterface $state,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    FieldTypeMWManager $field_type_mw_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_type_manager);

    $this->state = $state;
    $current_source_config = $migration->getSourceConfiguration();
    $this->bundle = $current_source_config['bundle'];

    if (isset($current_source_config['constants']['translations']) && $current_source_config['constants']['translations']) {
      $this->bundle = str_replace('_' . $current_source_config['constants']['langcode'], '', $current_source_config['bundle']);
    }
    $this->mwDatabase = $this->entityTypeManager->getStorage('mw_database')->load($configuration['mw_database']);
    $current_entity = $this->entityTypeManager->getStorage('origin_vocabulary')->load($this->bundle . '_' . $configuration['mw_database']);
    $this->currentConfig = $current_entity->get('shared_configuration');

    $this->translation = [Language::LANGCODE_NOT_SPECIFIED, 'es'];

    if (isset($this->currentConfig[array_key_first($this->currentConfig)]['languages'])) {
      $this->translation = [
        Language::LANGCODE_NOT_SPECIFIED,
        $this->currentConfig[array_key_first($this->currentConfig)]['languages']['default'],
      ];
    }

    if (isset($configuration['constants']['translations']) && $configuration['constants']['translations']) {
      $this->translation = [$configuration['constants']['langcode']];
    }
    $this->destinyType = $this->currentConfig['destiny_type'];
    $this->originType = $this->currentConfig['origin_type'];
    $this->configFactory = $config_factory;
    $this->fieldTypeMWManager = $field_type_mw_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, ?MigrationInterface $migration = NULL) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('plugin.manager.field_type_mw')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $prepare_row_result = parent::prepareRow($row);

    if ($row->hasSourceProperty('alias') && $row->getSourceProperty('alias') && strpos($row->getSourceProperty('alias'), '/') !== 0) {
      $row->setSourceProperty('alias', '/' . $row->getSourceProperty('alias'));
    }

    $queryAdded = FALSE;

    $query = $this->select('taxonomy_vocabulary', 'tv');
    $query->condition('tv.vid', $row->getSourceProperty('vid'));

    if ($this->destinyType === 'node') {
      $row->setSourceProperty('status', 1);
      $row->setSourceProperty('created', time());
    }

    $description[] = ['value' => $row->getSourceProperty('description')];
    $row->setSourceProperty('description', $description);

    foreach ($this->currentConfig[array_key_first($this->currentConfig)] as $field_name => $data_field) {
      if (isset($data_field['type']) && $data_field['destiny'] !== 'none') {
        switch ($data_field['type']) {
          case 'image':
            $new_value = [];
            foreach ($row->getSourceProperty($field_name) as $key => $value) {
              $query_uri = $this->select('file_managed', 'fm');
              $query_uri->fields('fm', ['uri']);
              $query_uri->leftJoin('field_data_field_file_image_alt_text', 'ffat', 'fm.fid = ffat.entity_id');
              $query_uri->fields('ffat', ['field_file_image_alt_text_value']);
              $query_uri->leftJoin('field_data_field_file_image_title_text', 'fftt', 'fm.fid = fftt.entity_id');
              $query_uri->fields('fftt', ['field_file_image_title_text_value']);
              $query_uri->condition('fm.fid', $value['fid']);
              $results = $query_uri->execute()->fetch();
              $new_value[$key] = $value;
              if ($results['field_file_image_alt_text_value']) {
                $new_value[0]['alt'] = $results['field_file_image_alt_text_value'];
              }
              if ($results['field_file_image_title_text_value']) {
                $new_value[0]['title'] = $results['field_file_image_title_text_value'];
              }
              $new_value[$key]['uri'] = $results['uri'];
              $new_value[$key]['type'] = $data_field['type'];
            }
            $row->setSourceProperty($field_name, $new_value);
            break;

          case 'file':
            $new_value = [];
            foreach ($row->getSourceProperty($field_name) as $key => $value) {
              $query_uri = $this->select('file_managed', 'fm');
              $query_uri->fields('fm', ['uri']);
              $query_uri->condition('fm.fid', $value['fid']);
              $uri = $query_uri->execute()->fetchField();
              $new_value[$key] = $value;
              $new_value[$key]['uri'] = $uri;
              $new_value[$key]['type'] = $data_field['type'];
            }
            $row->setSourceProperty($field_name, $new_value);
            break;

          case 'datetime':
            $date = $row->getSourceProperty($field_name . '_value');

            if (isset($data_field['format']) && $data_field['format'] !== '' && $date !== '') {
              $date = DrupalDateTime::createFromFormat('Y-m-d H:i:s', $date);
              $date->format($data_field['format']);
              $row->setSourceProperty($field_name . '_value', $date->format($data_field['format']));
            }
            break;

          case 'list_text':
            if ($data_field['destiny_type'] === 'entity_reference') {
              $this->listTextToTaxonomy($row, $field_name, $data_field);
            }
            break;
        }
      }
    }

    if ($queryAdded) {
      $valuesToRow = $this->query->execute()->fetchAll();
      foreach ($valuesToRow[0] as $key => $value) {
        if ($value !== '') {
          $row->setSourceProperty($key, $value);
        }
      }
    }

    return $prepare_row_result;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->leftTable = 'tv';
    $this->leftTableId = 'vid';
    $this->entityType = 'taxonomy_term';
    $this->leftJoinTable = $this->getLeftJoinTable();

    $query = parent::query();

    if ($this->currentConfig['origin_type'] === 'node') {
      $query->condition('n.language', $this->translation, 'in');
    }

    if (isset($this->currentConfig['import_path_alias']) && $this->currentConfig['import_path_alias'] == 1 && !\array_key_exists('ual', $query->getTables())) {
      $this->queryEntityUrlAlias($query);
    }

    if (isset($this->currentConfig['source_type_date_field']) && $this->currentConfig['source_type_date_field'] !== '') {
      $this->addFromCreate($query, strtotime($this->currentConfig['source_type_date_field']));
    }

    if (isset($this->currentConfig['import_only_ids']) && $this->currentConfig['import_only_ids'] !== '') {
      $this->importOnlyIds($query);
    }

    if (isset($this->currentConfig['limit_import']) && $this->currentConfig['limit_import'] !== '') {
      $this->limitImport($query);
    }

    return $query;
  }

  /**
   * Add date condition to query from create timestamp.
   */
  protected function addFromCreate(&$query, $timestamp): void {
    $query->condition('created', $timestamp, '>=');
  }

  /**
   * Get left join table string.
   */
  protected function getLeftJoinTable(): string {
    return "{$this->leftTable}.{$this->leftTableId}";
  }

  /**
   * Add to the query the condition to import only the ids specified.
   */
  protected function importOnlyIds(&$query): void {
    switch ($this->currentConfig['origin_type']) {
      case 'node':
        $query->condition('n.nid', explode(',', $this->currentConfig['import_only_ids']), 'IN');
        break;

      case 'taxonomy_term':
        $query->condition('ttd.tid', explode(',', $this->currentConfig['import_only_ids']), 'IN');
        break;

      case 'user':
        $query->condition('u.uid', explode(',', $this->currentConfig['import_only_ids']), 'IN');
        break;
    }
  }

  /**
   * Add to the query the limit to import.
   */
  protected function limitImport(&$query): void {
    $query->range(0, $this->currentConfig['limit_import']);

    $query->orderBy($this->leftJoinTable, 'ASC');
  }

  /**
   * Add to query the url alias.
   */
  protected function queryEntityUrlAlias(&$query): void {
    $translations = implode("','", $this->translation);
    $id__left = $this->leftJoinTable;

    if ($this->leftJoinTable === 'tv.vid') {
      $id__left = 'td.tid';
    }

    $source = $this->entityType;

    if ($this->entityType === 'taxonomy_term') {
      $source = 'taxonomy/term';
    }
    $query->leftJoin('url_alias', 'ual', "CONCAT('{$source}/', {$id__left}) = ual.source AND ual.pid = (SELECT MAX(ual2.pid) FROM url_alias as ual2 WHERE ual2.source=CONCAT('{$source}/', {$id__left})) AND ual.language in('{$translations}')");
    $query->fields('ual', ['alias']);
    $query->groupBy('ual.alias');
  }

  /**
   * This function will convert the value to taxonomy term.
   */
  private function listTextToTaxonomy(&$row, $field_name, $data_field): void {
    // Load taxonomy term.
    $term = $this->entityTypeManager->getStorage('taxonomy_term')->loadByProperties(['name' => $row->getSourceProperty($field_name . '_value')]);
    $term = reset($term);

    if ($term === FALSE) {
      $term = $this->entityTypeManager->getStorage('taxonomy_term')->create([
        'name' => $row->getSourceProperty($field_name . '_value'),
        'vid' => $data_field['destiny'],
      ]);
      $term->save();
    }
    $row->setSourceProperty($field_name . '_value', $term->id());
  }

}
