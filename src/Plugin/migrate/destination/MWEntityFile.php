<?php

namespace Drupal\migrate_wizard\Plugin\migrate\destination;

use Drupal\Core\Field\Plugin\Field\FieldType\UriItem;
use Drupal\migrate\MigrateException;
use Drupal\migrate\Plugin\migrate\destination\EntityContentBase;
use Drupal\migrate\Row;

/**
 * Provides a generic destination to import media files.
 *
 * @MigrateDestination(
 *     id="mwentity:file"
 * )
 */
class MWEntityFile extends EntityContentBase {

  /**
   * {@inheritdoc}
   */
  public function getEntity(Row $row, array $old_destination_id_values) {
    // For stub rows, there is no real file to deal with, let the stubbing
    // process take its default path.
    if ($row->isStub()) {
      return parent::getEntity($row, $old_destination_id_values);
    }

    // By default the entity key (fid) would be used, but we want to make sure
    // we're loading the matching URI.
    $destination = $row->getSourceProperty('uri');
    if (empty($destination)) {
      throw new MigrateException('Destination property uri not provided');
    }
    $entity = $this->storage->loadByProperties(['uri' => $destination]);

    if ($entity) {
      $entity = reset($entity);
    }
    else {
      $entity_id = reset($old_destination_id_values) ?: $this->getEntityId($row);

      if (!empty($entity_id) && ($entity = $this->storage->load($entity_id))) {
        // Allow updateEntity() to change the entity.
        $entity = $this->updateEntity($entity, $row) ?: $entity;
      }
      else {
        // Attempt to ensure we always have a bundle.
        if ($bundle = $this->getBundle($row)) {
          $row->setDestinationProperty($this->getKey('bundle'), $bundle);
        }

        // Stubs might need some required fields filled in.
        if ($row->isStub()) {
          $this->processStubRow($row);
        }

        // All neccesary data for create a file entity.
        $destination = [
          'filename' => $row->getSourceProperty('filename'),
          'source_full_path' => $row->getSourceProperty('source_base_path') . '/' . $row->getSourceProperty('filepath'),
          'uri' => $row->getSourceProperty('uri'),
          'filemime' => $row->getSourceProperty('filemime'),
          'status' => $row->getSourceProperty('status'),
          'created' => $row->getSourceProperty('timestamp'),
          'change' => $row->getSourceProperty('timestamp'),
          'uid' => $row->getSourceProperty('uid'),
        ];
        $entity = $this->storage->create($destination);
        $entity->enforceIsNew();
      }
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  protected function processStubRow(Row $row) {
    // We stub the uri value ourselves so we can create a real stub file for it.
    if (!$row->getDestinationProperty('uri')) {
      $field_definitions = $this->entityFieldManager
        ->getFieldDefinitions(
          $this->storage->getEntityTypeId(),
          $this->getKey('bundle')
        );
      $value = UriItem::generateSampleValue($field_definitions['uri']);

      if (empty($value)) {
        throw new MigrateException('Stubbing failed, unable to generate value for field uri');
      }
      // generateSampleValue() wraps the value in an array.
      $value = reset($value);
      // Make it into a proper public file uri, stripping off the existing
      // scheme if present.
      $value = 'public://' . preg_replace('|^[a-z]+://|i', '', $value);
      $value = mb_substr($value, 0, $field_definitions['uri']->getSetting('max_length'));
      // Create a real file, so File::preSave() can do filesize() on it.
      touch($value);
      $row->setDestinationProperty('uri', $value);
    }
    parent::processStubRow($row);
  }

}
