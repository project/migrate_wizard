<?php

namespace Drupal\migrate_wizard\Plugin\migrate\destination;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\entity_reference_revisions\Plugin\migrate\destination\EntityReferenceRevisions;
use Drupal\migrate\MigrateException;
use Drupal\migrate\Plugin\MigrateIdMapInterface;
use Drupal\migrate\Row;

/**
 * Provides entity_reference_revisions destination plugin.
 *
 * @MigrateDestination(
 *     id="mw_entity_reference_revisions",
 *     deriver="Drupal\migrate_wizard\Plugin\Derivative\MWMigrateEntityReferenceRevisions"
 * )
 */
class MWEntityReferenceRevisions extends EntityReferenceRevisions implements ConfigurableInterface {

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids = [];

    if ($revision_key = $this->getKey('revision')) {
      $id_key = $this->getKey('id');
      $ids[$id_key]['type'] = 'integer';

      $ids[$revision_key]['type'] = 'integer';

      if ($this->isTranslationDestination()) {
        if ($revision_key = $this->getKey('langcode')) {
          $ids[$revision_key]['type'] = 'string';
        }
        else {
          throw new MigrateException('This entity type does not support translation.');
        }
      }
    }

    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntity(Row $row, array $oldDestinationIdValues) {
    $entity_id = $oldDestinationIdValues ?
    array_shift($oldDestinationIdValues) :
    $this->getEntityId($row);

    if ($this->isTranslationDestination()) {
      $entity_id = $this->getEntityId($row);

      if (isset($entity_id[0])) {
        $entity_id = $entity_id[0];
      }
      else {
        $entity_id = FALSE;
      }
    }
    $configuration = $this->getConfiguration();

    if (isset($configuration['force_revision']) && $configuration['force_revision'] === TRUE) {
      $revision_id = NULL;
    }
    else {
      $revision_id = $oldDestinationIdValues ?
        array_pop($oldDestinationIdValues) :
        $row->getDestinationProperty($this->getKey('revision'));
    }

    // If a specific revision_id is supplied and exists, assert the entity_id
    // matches (if supplied), and update the revision.
    /** @var \Drupal\Core\Entity\RevisionableInterface|\Drupal\Core\Entity\EntityInterface $entity */
    if (!empty($revision_id) && ($entity = $this->storage->loadRevision($revision_id))) {
      if (!empty($entity_id) && ($entity->id() !== $entity_id)) {
        throw new MigrateException('The revision_id exists for this entity type, but does not belong to the given entity id');
      }
      $entity = $this->updateEntity($entity, $row) ?: $entity;
    }
    // If there is no revision_id supplied, but there is an entity_id
    // supplied that exists, update it.
    elseif (!empty($entity_id) && ($entity = $this->storage->load($entity_id))) {
      // If so configured, create a new revision while updating.
      if (!empty($this->configuration['new_revisions'])) {
        $entity->setNewRevision(TRUE);
      }
      $entity = $this->updateEntity($entity, $row) ?: $entity;
    }

    // Otherwise, create a new (possibly stub) entity.
    else {
      // Attempt to ensure we always have a bundle.
      if ($bundle = $this->getBundle($row)) {
        $row->setDestinationProperty($this->getKey('bundle'), $bundle);
      }

      // Stubs might need some required fields filled in.
      if ($row->isStub()) {
        $this->processStubRow($row);
      }
      $entity = $this->storage->create($row->getDestination())
        ->enforceIsNew(TRUE);
    }
    $this->rollbackAction = MigrateIdMapInterface::ROLLBACK_DELETE;

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  protected static function getEntityTypeId($pluginId) {
    // Remove "entity_reference_revisions:".
    // Ideally, we would call getDerivativeId(), but since this is static
    // that is not possible so we follow the same pattern as core.
    return substr($pluginId, 30);
  }

}
