<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

/**
 * Provides 'range_integer' field type.
 *
 * @FieldType(
 *     id="range_integer",
 * )
 */
class FieldTypeMWRangeInteger extends FieldTypeMWGenericRange {

}
