<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

/**
 * Provides 'text' field type.
 *
 * @FieldType(
 *     id="text",
 * )
 */
class FieldTypeMWText extends FieldTypeMWGenericText {

}
