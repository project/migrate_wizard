<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

use Drupal\migrate_wizard\FieldTypeMWBase;

/**
 * Provides 'default_paragraph_item' field type.
 *
 * @FieldType(
 *     id="default_paragraph_item",
 * )
 */
class FieldTypeMWDefaultParagraphItem extends FieldTypeMWBase {

  /**
   * Generate the base of migration of paragraph items.
   *
   * @param array $wildcard_yml_config
   *   The configuration of the migration.
   * @param array $array_config
   *   The configuration of the field.
   * @param string $origin_node
   *   The origin node.
   * @param string $migration_group_name
   *   The migration group name.
   * @param string $default_langcode
   *   The default language code.
   * @param null $langcode
   *   The language code.
   * @param bool $translation
   *   The translation flag.
   */
  public static function getD7MigrationConfig(&$wildcard_yml_config, $array_config, $origin_node, $migration_group_name, $default_langcode = NULL, $langcode = NULL, $translation = FALSE): void {
    $mw_database = parent::getMwDatabase($array_config['mw_database']);
    $suffix = '';

    if ($translation) {
      $suffix = '_' . $langcode;
    }
    $wildcard_yml_config['id'] = $origin_node . '_' . $array_config['mw_database'] . $suffix;
    $wildcard_yml_config['label'] = $origin_node . $suffix;
    $wildcard_yml_config['process']['langcode'][] = [
      'plugin' => 'skip_on_empty',
      'source' => 'language',
      'method' => 'row',
    ];
    $wildcard_yml_config['migration_group'] = $migration_group_name;
    $wildcard_yml_config['migration_tags'][0] = $migration_group_name;
    $wildcard_yml_config['source']['key'] = $mw_database->get('key');
    $wildcard_yml_config['source']['mw_database'] = $array_config['mw_database'];
    $wildcard_yml_config['source']['bundle'] = $origin_node;
    $wildcard_yml_config['source']['field_name'] = $origin_node;
    $wildcard_yml_config['source']['name'] = $origin_node;
    $wildcard_yml_config['source']['origin_type'] = $array_config['origin_type'];
    $wildcard_yml_config['source']['entity_type'] = $array_config['origin_type'];
    $wildcard_yml_config['source']['constants']['langcode'] = $langcode;
    $wildcard_yml_config['source']['constants']['default_langcode'] = $default_langcode;

    if ($translation) {
      $wildcard_yml_config['source']['translations'] = TRUE;
      $wildcard_yml_config['source']['constants']['translations'] = TRUE;
      $wildcard_yml_config['process']['id'][] = [
        'plugin' => 'mw_fc_translation_migration_lookup',
        'migration' => $origin_node . '_' . $mw_database->id,
        'source' => 'id_lookup',
        'no_stub' => TRUE,
      ];
      $wildcard_yml_config['destination']['default_bundle'] = $origin_node;
      $wildcard_yml_config['destination']['translations'] = TRUE;
      $wildcard_yml_config['destination']['langcode'] = $langcode;
    }
    $wildcard_yml_config['process']['type'][] = [
      'plugin' => 'default_value',
      'default_value' => array_key_first($array_config),
    ];
    $wildcard_yml_config['process']['created'] = 'created';

    if (isset($array_config['preserve_nid']) && $array_config['preserve_nid'] && !$translation) {
      $wildcard_yml_config['process']['nid'] = 'nid';
    }
  }

}
