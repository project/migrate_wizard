<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

/**
 * Provides 'image' field type.
 *
 * @FieldType(
 *     id="image",
 * )
 */
class FieldTypeMWImage extends FieldTypeMWGenericFile {

}
