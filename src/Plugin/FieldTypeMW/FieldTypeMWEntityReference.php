<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

/**
 * Provides 'entityreference' field type.
 *
 * @FieldType(
 *     id="entityreference",
 * )
 */
class FieldTypeMWEntityReference extends FieldTypeMWGenericReference {

}
