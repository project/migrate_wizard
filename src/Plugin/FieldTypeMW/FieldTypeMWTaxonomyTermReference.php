<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

/**
 * Provides 'taxonomy_term_reference' field type.
 *
 * @FieldType(
 *     id="taxonomy_term_reference",
 * )
 */
class FieldTypeMWTaxonomyTermReference extends FieldTypeMWGenericReference {

}
