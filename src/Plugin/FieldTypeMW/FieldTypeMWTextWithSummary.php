<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

/**
 * Provides 'text_with_summary' field type.
 *
 * @FieldType(
 *     id="text_with_summary",
 * )
 */
class FieldTypeMWTextWithSummary extends FieldTypeMWGenericTextLong {

}
