<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

/**
 * Provides 'list_text' field type.
 *
 * @FieldType(
 *     id="list_text",
 * )
 */
class FieldTypeMWListText extends FieldTypeMWGenericText {

}
