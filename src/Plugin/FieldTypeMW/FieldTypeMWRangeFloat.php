<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

/**
 * Provides 'range_float' field type.
 *
 * @FieldType(
 *     id="range_float",
 * )
 */
class FieldTypeMWRangeFloat extends FieldTypeMWGenericRange {

}
