<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

/**
 * Provides 'text_long' field type.
 *
 * @FieldType(
 *     id="text_long",
 * )
 */
class FieldTypeMWTextLong extends FieldTypeMWGenericTextLong {

}
