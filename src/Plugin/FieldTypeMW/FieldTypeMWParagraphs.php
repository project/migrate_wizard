<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

use Drupal\migrate_wizard\FieldTypeMWBase;

/**
 * Provides 'paragraphs' field type.
 *
 * @FieldType(
 *     id="paragraphs",
 * )
 */
class FieldTypeMWParagraphs extends FieldTypeMWBase {

  /**
   * Generate the migration of paragraph field type.
   *
   * @param array $wildcard_yml_config
   *   The configuration of the migration.
   * @param array $origin_field
   *   The origin field.
   * @param array $data_field
   *   The data field.
   * @param string $type_source
   *   The type source.
   * @param array $config
   *   The configuration.
   * @param object $entityFieldManager
   *   The entity field manager.
   * @param object $fieldTypeMWManager
   *   The field type manager.
   * @param string|null $langcode
   *   The langcode.
   */
  public static function getD7MigrationConfig(&$wildcard_yml_config, $origin_field, $data_field, $type_source, $config, $entityFieldManager, $fieldTypeMWManager, $langcode = NULL): void {
    $mw_database = parent::getMwDatabase($wildcard_yml_config['source']['mw_database']);
    $suffix = '';

    if (isset($wildcard_yml_config['source']['constants']['translations']) && $wildcard_yml_config['source']['constants']['translations']) {
      $suffix = '_' . $langcode;
    }
    $destiny_explode = explode('/', $data_field['destiny']);
    $array_config['pseudo_'.$origin_field][] = [
      'plugin' => 'skip_on_empty',
      'method' => 'process',
      'source' => $origin_field,
    ];

    foreach ($data_field['bundle'] as $bundle) {
      $array_config['pseudo_'.$origin_field][] = [
        'plugin' => 'migration_lookup',
        'migration' => $bundle . '_' . $mw_database->id . $suffix,
        'no_stub' => TRUE,
      ];

    }
    if(!isset($wildcard_yml_config["process"][$destiny_explode[0]])){

      $array_config['pseudo_'.$destiny_explode[0].'_destiny'][] = [
        'plugin' => 'concat_data',
        'source' => ['@pseudo_'.$origin_field]
      ];

      $array_config[$destiny_explode[0]][] = [
        'plugin' => 'sub_process',
        'source' => '@pseudo_'.$destiny_explode[0].'_destiny',
        'process' => ['target_id' => '0', 'target_revision_id' => '1'],
      ];
      $wildcard_yml_config['process'] = array_merge($wildcard_yml_config['process'], $array_config);
    }else{
      foreach(reset($array_config) as $insert_value){
        $wildcard_yml_config['process'] = parent::InsertBeforeKey($wildcard_yml_config['process'], 'pseudo_' . $destiny_explode[0] . '_destiny', 'pseudo_' . $origin_field, $insert_value);
      }
      $wildcard_yml_config['process']['pseudo_' . $destiny_explode[0] . '_destiny'][0]['source'][] = '@pseudo_' . $origin_field;
    }
    $wildcard_yml_config['migration_dependencies']['required'][] = $bundle . '_' . $mw_database->id;
  }

}
