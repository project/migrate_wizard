<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

/**
 * Provides 'list_boolean' field type.
 *
 * @FieldType(
 *     id="list_boolean",
 * )
 */
class FieldTypeMWListBoolean extends FieldTypeMWGenericText {

}
