<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

use Drupal\migrate_wizard\FieldTypeMWBase;

/**
 * Provides 'workbench_access_node' field type.
 *
 * @FieldType(
 *     id="workbench_access_node",
 * )
 */
class FieldTypeMWWorkbenchAccessNode extends FieldTypeMWBase {

  /**
   * Generate the migration of workbench field type.
   *
   * @param array $wildcard_yml_config
   *   The configuration of the migration.
   * @param array $origin_field
   *   The origin field.
   * @param array $data_field
   *   The data field.
   * @param string $type_source
   *   The type source.
   * @param array $config
   *   The configuration.
   * @param object $entityFieldManager
   *   The entity field manager.
   * @param object $fieldTypeMWManager
   *   The field type manager.
   * @param string|null $langcode
   *   The langcode.
   */
  public static function getMigrationConfig(&$wildcard_yml_config, $origin_field, $data_field, $type_source, $config, $entityFieldManager, $fieldTypeMWManager, $langcode = NULL): void {
    $wildcard_yml_config['process'][$data_field['destiny']][] = [
      'plugin' => 'skip_on_empty',
      'source' => 'access_id',
      'method' => 'process',
    ];
    $wildcard_yml_config['process'][$data_field['destiny']][] = [
      'plugin' => 'migration_lookup',
      'migration' => $data_field['migration_lookup'],
      'no_stub' => TRUE,
    ];
    $wildcard_yml_config['migration_dependencies']['required'][] = $data_field['migration_lookup'];
  }

}
