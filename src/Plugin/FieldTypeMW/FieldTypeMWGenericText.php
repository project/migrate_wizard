<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

use Drupal\migrate_wizard\FieldTypeMWBase;

/**
 * Provides 'generic_text' field type.
 *
 * @FieldType(
 *     id="generic_text",
 * )
 */
class FieldTypeMWGenericText extends FieldTypeMWBase {

  /**
   * Generate the migration of text field type.
   *
   * @param array $wildcard_yml_config
   *   The configuration of the migration.
   * @param array $origin_field
   *   The origin field.
   * @param array $data_field
   *   The data field.
   * @param string $type_source
   *   The type source.
   * @param array $config
   *   The configuration.
   * @param object $entityFieldManager
   *   The entity field manager.
   * @param object $fieldTypeMWManager
   *   The field type manager.
   * @param string|null $langcode
   *   The langcode.
   */
  public static function getD7MigrationConfig(&$wildcard_yml_config, $origin_field, $data_field, $type_source, $config, $entityFieldManager, $fieldTypeMWManager, $langcode = NULL): void {
    $method = 'process';

    if (($data_field['destiny'] === 'title' && $wildcard_yml_config['destination']['plugin'] === 'entity:node')
        || ($data_field['destiny'] === 'name' && $wildcard_yml_config['destination']['plugin'] === 'entity:taxonomy_term')) {
      $method = 'row';
    }

    $sourceName = $origin_field;

    if ($data_field['destiny'] === 'name' && $wildcard_yml_config['source']['origin_type'] === 'vocabulary' && $langcode !== NULL) {
      $sourceName = 'name_translated';
    }

    $array_config[$data_field['destiny']][] = [
      'plugin' => 'skip_on_empty',
      'source' => $sourceName,
      'method' => $method,
    ];

    if ($sourceName !== 'title' && $sourceName !== 'name' && $sourceName !== 'name_translated') {
      $array_config[$data_field['destiny']][] = [
        'plugin' => 'sub_process',
        'process' => [
          'value' => 'value',
          'format' => 'format',
        ],
      ];
    }

    $wildcard_yml_config['process'] = array_merge($wildcard_yml_config['process'], $array_config);
  }

}
