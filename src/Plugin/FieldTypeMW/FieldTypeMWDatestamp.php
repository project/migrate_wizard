<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

/**
 * Provides 'datestamp' field type.
 *
 * @FieldType(
 *     id="datestamp",
 * )
 */
class FieldTypeMWDatestamp extends FieldTypeMWGenericDate {

}
