<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

/**
 * Provides 'number_float' field type.
 *
 * @FieldType(
 *     id="number_float",
 * )
 */
class FieldTypeMWNumberFloat extends FieldTypeMWGenericText {

}
