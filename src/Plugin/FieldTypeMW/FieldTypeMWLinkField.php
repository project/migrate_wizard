<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

use Drupal\migrate_wizard\FieldTypeMWBase;

/**
 * Provides 'link_field' field type.
 *
 * @FieldType(
 *     id="link_field",
 * )
 */
class FieldTypeMWLinkField extends FieldTypeMWBase {

  /**
   * Generate the migration of link field type.
   *
   * @param array $wildcard_yml_config
   *   The configuration of the migration.
   * @param array $origin_field
   *   The origin field.
   * @param array $data_field
   *   The data field.
   * @param string $type_source
   *   The type source.
   * @param array $config
   *   The configuration.
   * @param object $entityFieldManager
   *   The entity field manager.
   * @param object $fieldTypeMWManager
   *   The field type manager.
   * @param string|null $langcode
   *   The langcode.
   */
  public static function getD7MigrationConfig(&$wildcard_yml_config, $origin_field, $data_field, $type_source, $config, $entityFieldManager, $fieldTypeMWManager, $langcode = NULL): void {
    $array_config = [];

    if ($data_field['destiny'] === 'social_media_links') {
      if (!isset($wildcard_yml_config['process']['pseudo_' . $data_field['destiny']])) {
        $array_config['pseudo_' . $origin_field][] = [
          'plugin' => 'skip_on_empty',
          'source' => $origin_field,
          'method' => 'process',
        ];
        $array_config['pseudo_' . $origin_field][] = [
          'plugin' => 'social_media_links',
        ];
        $array_config['pseudo_' . $data_field['destiny']][] = [
          'plugin' => 'skip_on_empty',
          'source' => ['@pseudo_' . $origin_field],
          'method' => 'process',
        ];
        $array_config['pseudo_' . $data_field['destiny']][] = [
          'plugin' => 'reset_arrays',
        ];
        $array_config[$data_field['destiny']][] = [
          'plugin' => 'skip_on_empty',
          'source' => '@pseudo_' . $data_field['destiny'],
          'method' => 'process',
        ];
        $array_config[$data_field['destiny']][] = [
          'plugin' => 'sub_process',
          'method' => 'process',
          'process' => ['link' => 'link', 'social' => 'social'],
        ];
      }
      else {
        $insert_value = [
          'plugin' => 'skip_on_empty',
          'source' => $origin_field,
          'method' => 'process',
        ];
        $wildcard_yml_config['process'] = parent::insertBeforeKey($wildcard_yml_config['process'], 'pseudo_' . $data_field['destiny'], 'pseudo_' . $origin_field, $insert_value);
        $wildcard_yml_config['process']['pseudo_' . $origin_field][] = [
          'plugin' => 'social_media_links',
        ];
        $wildcard_yml_config['process']['pseudo_' . $data_field['destiny']][0]['source'][] = '@pseudo_' . $origin_field;
      }
    }
    else {
      // @todo unserialize attributes d7, map to d10 and serialize
      $array_config[$data_field['destiny']][] = [
        'plugin' => 'skip_on_empty',
        'source' => $origin_field,
        'method' => 'process',
      ];
      $array_config[$data_field['destiny']][] = [
        'plugin' => 'sub_process',
        'process' => [
          'uri' => 'url',
          'title' => 'title',
        ],
      ];
    }
    $wildcard_yml_config['process'] = array_merge($wildcard_yml_config['process'], $array_config);
  }

}
