<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

use Drupal\migrate_wizard\FieldTypeMWBase;

/**
 * Provides 'default_vocabulary' field type.
 *
 * @FieldType(
 *     id="default_vocabulary",
 * )
 */
class FieldTypeMWDefaultVocabulary extends FieldTypeMWBase {

  /**
   * Generate the base of migration of vocabularies.
   *
   * @param array $wildcard_yml_config
   *   The configuration of the migration.
   * @param array $array_config
   *   The configuration of the field.
   * @param string $origin_node
   *   The origin node.
   * @param string $migration_group_name
   *   The migration group name.
   * @param string $default_langcode
   *   The default language code.
   * @param null $langcode
   *   The language code.
   * @param bool $translation
   *   The translation flag.
   */
  public static function getD7MigrationConfig(&$wildcard_yml_config, $array_config, $origin_node, $migration_group_name, $default_langcode = NULL, $langcode = NULL, $translation = FALSE) {
    $mw_database = parent::getMwDatabase($array_config['mw_database']);
    $suffix = '';

    if ($translation) {
      $suffix = '_' . $langcode;
    }
    $wildcard_yml_config['langcode'] = $langcode;
    $wildcard_yml_config['id'] = $origin_node . '_' . $array_config['mw_database'] . $suffix;
    $wildcard_yml_config['label'] = $origin_node . $suffix;
    $wildcard_yml_config['migration_group'] = $migration_group_name;
    $wildcard_yml_config['migration_tags'][0] = $migration_group_name;
    $wildcard_yml_config['source']['key'] = $mw_database->get('key');
    $database = $mw_database->get('id');
    $wildcard_yml_config['source']['mw_database'] = $database;
    $wildcard_yml_config['source']['bundle'] = $origin_node;
    $wildcard_yml_config['source']['origin_type'] = $array_config['origin_type'];
    $wildcard_yml_config['process']['vid'][0]['default_value'] = array_key_first($array_config);
    $wildcard_yml_config['process']['parent_id'][1]['migration'] = $origin_node . '_' . $array_config['mw_database'] . $suffix;
    $wildcard_yml_config['destination']['plugin'] = 'entity:taxonomy_term';
    $wildcard_yml_config['source']['constants']['langcode'] = $langcode;

    if (isset($array_config['preserve_tid']) && $array_config['preserve_tid']) {
      $wildcard_yml_config['process']['tid'] = 'tid';
    }

    if ($translation) {
      $wildcard_yml_config['source']['plugin'] = 'mw_d7_term_localized_translation';

      switch ($array_config['i18n_mode']) {
        case 1:
          $wildcard_yml_config['process']['tid'] = [
            'plugin' => 'migration_lookup',
            'migration' => $origin_node . '_' . $array_config['mw_database'],
            'source' => 'tid',
            'no_stub' => TRUE,
          ];

          break;

        case 4:
          $wildcard_yml_config['process']['tid'] = [
            'plugin' => 'migration_lookup',
            'migration' => $origin_node . '_' . $array_config['mw_database'],
            'source' => 'ttd_2_tid',
            'no_stub' => TRUE,
          ];

          break;
      }
      $wildcard_yml_config['source']['translations'] = TRUE;
      $wildcard_yml_config['source']['constants']['translations'] = TRUE;

      $wildcard_yml_config['process']['langcode'] = [
        'plugin' => 'default_value',
        'default_value' => $langcode,
      ];
      $wildcard_yml_config['destination']['default_bundle'] = $origin_node;
      $wildcard_yml_config['destination']['translations'] = TRUE;
      $wildcard_yml_config['destination']['langcode'] = $langcode;
      $wildcard_yml_config['langcode'] = $langcode;
      $wildcard_yml_config['migration_dependencies']['required'] = [$origin_node . '_' . $array_config['mw_database']];
    }
    $wildcard_yml_config['process']['type'] = [
      'plugin' => 'default_value',
      'default_value' => array_key_first($array_config),
    ];

    if ($array_config['origin_type'] === 'node') {
      $wildcard_yml_config['process']['weight'][0]['source'] = 'nid';
    }

    if ($array_config['import_path_alias']) {
      $wildcard_yml_config['process']['path/pathauto'][] = [
        'plugin' => 'default_value',
        'default_value' => 0,
      ];
      $wildcard_yml_config['process']['path/alias'][] = [
        'plugin' => 'skip_on_empty',
        'source' => 'alias',
        'method' => 'process',
      ];
    }

    if ($array_config['destiny_type'] === 'node') {
      $wildcard_yml_config['process']['type'] = [
        'plugin' => 'default_value',
        'default_value' => array_key_first($array_config),
      ];
      $wildcard_yml_config['destination']['plugin'] = 'entity:node';
      unset($wildcard_yml_config['process']['vid'], $wildcard_yml_config['process']['name'], $wildcard_yml_config['process']['changed'], $wildcard_yml_config['process']['weight'], $wildcard_yml_config['process']['parent_id'], $wildcard_yml_config['process']['parent']);
    }
  }

}
