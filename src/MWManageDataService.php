<?php

namespace Drupal\migrate_wizard;

use Drupal\Component\Transliteration\PhpTransliteration;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigManager;
use Drupal\Core\Database\Database;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionList;
use Drupal\Core\Language\LanguageInterface;
use Drupal\migrate\Plugin\MigrationDeriverTrait;
use Drupal\migrate_drupal\Plugin\migrate\source\d7\FieldableEntity;
use Drupal\user\Entity\Role;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Defines a Service to update nodes.
 */
class MWManageDataService extends FieldableEntity {

  use MigrationDeriverTrait;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  public $entityFieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  public $entityTypeManager;

  /**
   * The configuration object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The configuration manager.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;

  /**
   * The extension list module.
   *
   * @var \Drupal\Core\Extension\ExtensionList
   */
  protected $extensionListModule;

  /**
   * The current mw_database.
   *
   * @var \Drupal\migrate_wizard\Entity\MWDatabase
   */
  protected $mwDatabase;

  /**
   * The transliterator.
   *
   * @var \Drupal\Component\Transliteration\PhpTransliteration
   */
  protected $transliterator;

  /**
   * MWManageDataService constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   * @param \Drupal\Core\Extension\ExtensionList $extension_list_module
   *   The extension list module.
   * @param \Drupal\Core\Config\ConfigManager $config_manager
   *   The configuration manager.
   * @param \Drupal\Component\Transliteration\PhpTransliteration $php_transliteration
   *   The transliterator.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(
    EntityFieldManagerInterface $entity_field_manager,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $configFactory,
    ExtensionList $extension_list_module,
    ConfigManager $config_manager,
    PhpTransliteration $php_transliteration,
    RequestStack $request_stack
  ) {
    $this->config = $configFactory->getEditable('migrate_wizard.settings');
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->extensionListModule = $extension_list_module;
    $this->configManager = $config_manager;
    $this->transliterator = $php_transliteration;
    $this->mwDatabase = $request_stack->getCurrentRequest()->get('mw_database');

    // @todo refactor this to a public method and use in source migration plugins
    if ($this->mwDatabase !== NULL) {
      $info = [
        'host' => $this->mwDatabase->get('use_env') ? getenv($this->mwDatabase->get('host')) : $this->mwDatabase->get('host'),
        'database' => $this->mwDatabase->get('use_env') ? getenv($this->mwDatabase->get('database')) : $this->mwDatabase->get('database'),
        'username' => $this->mwDatabase->get('use_env') ? getenv($this->mwDatabase->get('username')) : $this->mwDatabase->get('username'),
        'password' => $this->mwDatabase->get('use_env') ? getenv($this->mwDatabase->get('password')) : $this->mwDatabase->get('password'),
        'prefix' => $this->mwDatabase->get('prefix'),
        'driver' => $this->mwDatabase->get('driver'),
        'port' => $this->mwDatabase->get('port'),
        'namespace' => $this->mwDatabase->get('namespace'),
        'collation' => $this->mwDatabase->get('collation'),
      ];
      Database::addConnectionInfo($this->mwDatabase->get('key'), $this->mwDatabase->get('target'), $info);
      Database::setActiveConnection($this->mwDatabase->get('key'));
      $this->configuration['key'] = $this->mwDatabase->get('key');
      $this->configuration['target'] = $this->mwDatabase->get('target');
    }
  }

  /**
   * Check the source database to see if the cas module is installed.
   */
  public function checkIfCasModuleOriginExistsEnabled() {
    $query = $this->select('system', 's')
      ->fields('s', ['status'])
      ->condition('s.name', 'cas');

    return $query->execute()->fetchField();
  }

  /**
   * Check source database to see if the workbench access module is installed.
   */
  public function checkWorkbenchAccessNodeExist() {
    $query = $this->select('system', 's')
      ->fields('s', ['status'])
      ->condition('s.status', 1)
      ->condition('s.name', 'workbench_access');

    return $query->execute()->fetchField();
  }

  /**
   * Clear all entities of type origin_field_collection_item.
   *
   * @return \Drupal\migrate_wizard\Entity\MWDatabase
   *   The current database.
   */
  public function clearOriginFieldCollectionItems() {
    // Delete all entities of type origin_node.
    $entity = $this->entityTypeManager->getStorage('origin_field_collection_item')->loadMultiple();
    $this->entityTypeManager->getStorage('origin_field_collection_item')->delete($entity);

    return $this->mwDatabase;
  }

  /**
   * Clear all entities of type origin_media.
   *
   * @return \Drupal\migrate_wizard\Entity\MWDatabase
   *   The current database.
   */
  public function clearOriginMedias() {
    $entity = $this->entityTypeManager->getStorage('origin_media')->loadByProperties(['database' => $this->mwDatabase->get('id')]);
    $this->entityTypeManager->getStorage('origin_media')->delete($entity);

    return $this->mwDatabase;
  }

  /**
   * Clear all entities of type origin_node.
   *
   * @return \Drupal\migrate_wizard\Entity\MWDatabase
   *   The current database.
   */
  public function clearOriginNodes() {
    $entity = $this->entityTypeManager->getStorage('origin_node')->loadByProperties(['database' => $this->mwDatabase->get('id')]);
    $this->entityTypeManager->getStorage('origin_node')->delete($entity);

    return $this->mwDatabase;
  }

  /**
   * Clear all entities of type origin_paragraph_item.
   *
   * @return \Drupal\migrate_wizard\Entity\MWDatabase
   *   The current database.
   */
  public function clearOriginParagraphs() {
    // Delete all entities of type origin_node.
    $entity = $this->entityTypeManager->getStorage('origin_paragraph_item')->loadMultiple();
    $this->entityTypeManager->getStorage('origin_paragraph_item')->delete($entity);

    return $this->mwDatabase;
  }

  /**
   * Clear all entities of type origin_vocabulary.
   *
   * @return \Drupal\migrate_wizard\Entity\MWDatabase
   *   The current database.
   */
  public function clearOriginVocabularies() {
    $entity = $this->entityTypeManager->getStorage('origin_vocabulary')->loadByProperties(['database' => $this->mwDatabase->get('id')]);
    $this->entityTypeManager->getStorage('origin_vocabulary')->delete($entity);

    return $this->mwDatabase;
  }

  /**
   * Create the selected roles for such action.
   *
   * @param array $array_config
   *   The configuration array.
   */
  public function createRoles(&$array_config): void {
    foreach ($array_config['roles'] as &$role) {
      if ($role['destiny'] === 'create') {
        $data = [
          'id' => $this->transliterateString($role['origin']),
          'label' => $role['origin'],
        ];
        $role_obj = Role::create($data);
        $role_obj->save();
        $role['destiny'] = $role_obj->id();
      }
    }
  }

  /**
   * Get the content types.
   *
   * @return array
   *   The content types.
   */
  public function getCurrentContentTypes(): array {
    $content_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    $content_types_array = [];
    foreach ($content_types as $content_type) {
      $content_types_array[$content_type->id()] = $content_type->label();
    }

    return $content_types_array;
  }

  /**
   * Get the current vocabularies.
   *
   * @return array
   *   The vocabularies.
   */
  public function getCurrentVocabularies(): array {
    $vocabularies = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple();
    $vocabularies_array = [];

    foreach ($vocabularies as $vocabularie) {
      $vocabularies_array[$vocabularie->id()] = $vocabularie->label();
    }

    return $vocabularies_array;
  }

  /**
   * Returns the current database.
   *
   * @return \Drupal\Core\Database\Connection
   *   The current database.
   */
  public function getDatabase() {
    if (isset($this->configuration['key'], $this->configuration['target'])) {
      $this->database = $this->setUpDatabase([
        'key' => $this->configuration['key'],
        'target' => $this->configuration['target'],
      ]);
    }
    elseif (!isset($this->database)) {
      $this->database = $this->setUpDatabase([]);
    }

    return $this->database;
  }

  /**
   * Returns the options for the detiny media field selectors.
   *
   * @param string|null $bundle
   *   The bundle.
   *
   * @return array
   *   The fields.
   */
  public function getFieldsMediaByBundle(?string $bundle = NULL): array {
    $fieldsArrayMedia = [];

    if ($bundle !== NULL) {
      $fieldMediaDefinitions = $this->entityFieldManager->getFieldDefinitions('media', $bundle);
      $fieldMediaBaseDefinitions = $this->entityFieldManager->getBaseFieldDefinitions('media');
      $fieldsExtraMedia = array_diff_key($fieldMediaDefinitions, $fieldMediaBaseDefinitions);

      foreach ($fieldsExtraMedia as $fieldExtraMedia) {
        if (!isset($fieldsArrayMedia[$fieldExtraMedia->getName()])) {
          $fieldsArrayMedia[$fieldExtraMedia->getName()] = $fieldExtraMedia->getLabel();
        }
      }
    }

    return $fieldsArrayMedia;
  }

  /**
   * Returns the fields of a source content.
   *
   * @param string $origin_bundle
   *   The origin bundle.
   * @param string $origin_type
   *   The origin type.
   *
   * @return array
   *   The fields.
   */
  public function getFieldsOriginBundle($origin_bundle, $origin_type): array {
    $this->configuration['key'] = $origin_bundle->key;
    $this->configuration['target'] = $origin_bundle->target;
    $query = $this->select('field_config_instance', 'fci')
      ->fields('fci', ['field_id'])
      ->fields('fci', ['field_name'])
      ->fields('fci', ['data'])
      ->condition('fci.entity_type', $origin_type)
      ->condition('fci.bundle', $origin_bundle->id_origin)
      ->condition('fci.deleted', 0);
    $query->leftJoin('field_config', 'fc', 'fci.field_id = fc.id');
    $query->fields('fc', [
      'type',
      'data',
    ]);

    return $query->execute()->fetchAll();
  }

  /**
   * Get the media types on destiny.
   *
   * @return array
   *   The media types.
   */
  public function getMediaTypes(): array {
    $media_types = $this->entityTypeManager->getListBuilder('media_type')->load();
    $fieldsArrayMediaTypes = [];
    foreach ($media_types as $media_type) {
      $fieldsArrayMediaTypes[$media_type->id()] = $media_type->label();
    }
    return $fieldsArrayMediaTypes;
  }

  /**
   * Get the current Database.
   *
   * @return \Drupal\migrate_wizard\Entity\MWDatabase
   *   The current database.
   */
  public function getMwDatabase() {
    return $this->mwDatabase;
  }

  /**
   * Return the title name of the origin bundle.
   *
   * @param string $bundle
   *   The bundle.
   * @param string $type
   *   The type.
   *
   * @return mixed
   *   The title.
   */
  public function getOriginBundleTitle($bundle, $type) {
    $query = [];
    $title = '';
    switch ($type) {
      case 'node':
        $query = $this->select('node_type', 'nt')
          ->fields('nt', ['name'])
          ->condition('nt.type', $bundle);
        $title = $query->execute()->fetchField();
        break;

      case 'taxonomy_term':
        $query = $this->select('taxonomy_vocabulary', 'tv')
          ->fields('tv', ['name'])
          ->condition('tv.machine_name', $bundle);
        $title = $query->execute()->fetchField();
        break;

      case 'user':
        $query = $this->select('role', 'r')
          ->fields('r', ['name'])
          ->condition('r.rid', $bundle);
        $title = $query->execute()->fetchField();
        break;

      case 'media':
        $query = $this->select('role', 'r')
          ->fields('r', ['name'])
          ->condition('r.rid', $bundle);
        $title = $bundle;
        break;
    }

    return $title;
  }

  /**
   * Return the default language of the origin.
   *
   * @param string|null $current_config
   *   The current config.
   *
   * @return mixed
   *   The default language.
   */
  public function getOriginDefaultLangcode($current_config = NULL) {
    if ($current_config) {
      $this->config->set('migrate_wizard_settings', $current_config)->save();
    }
    $query = $this->select('variable', 'v')
      ->fields('v', ['value'])
      ->condition('v.name', 'language_default');
    $results_query = $query->execute()->fetchField();
    $results = unserialize($results_query, ['allowed_classes' => ['stdClass']]);

    return $results->language;
  }

  /**
   * Get the origin editor formats.
   *
   * @return array
   *   The editor formats.
   */
  public function getOriginEditorFormats(): array {
    $query = $this->select('filter_format', 'ff')
      ->fields('ff', ['name'])
      ->fields('ff', ['format']);

    return $query->execute()->fetchAll();
  }

  /**
   * Get the origin languages.
   *
   * @param string $origin_type
   *   The origin type.
   * @param string $origin_bundle
   *   The origin bundle.
   *
   * @return array
   *   The origin languages.
   */
  public function getOriginLanguages($origin_type, $origin_bundle): array {
    $results['origin_languages'] = [];
    switch ($origin_type) {
      case 'node':
        $query_language_content_type = $this->select('variable', 'v')
          ->fields('v', ['value'])
          ->condition('v.name', 'language_content_type_' . $origin_bundle);
        $results_language_content_type = $query_language_content_type->execute()->fetchField();
        $language_content_type = unserialize($results_language_content_type, ['allowed_classes' => ['stdClass']]);
        if ($language_content_type > 0) {
          $results['origin_languages'] = $this->getLanguageTranslations();
        }
        break;

      case 'vocabulary':
        if ($this->database->schema()->fieldExists('taxonomy_vocabulary', 'i18n_mode')) {
          $query_type_i18n_mode = $this->select('taxonomy_vocabulary', 'tv')
            ->fields('tv', ['i18n_mode', 'language'])
            ->condition('tv.machine_name', $origin_bundle);
          $query_type_i18n_mode = $query_type_i18n_mode->execute()->fetchAll();
          $results['i18n_mode'] = $query_type_i18n_mode[0]['i18n_mode'];
          $results['language'] = $query_type_i18n_mode[0]['language'];
          if ($results['i18n_mode'] !== 0) {
            $results['origin_languages'] = $this->getLanguageTranslations();
          }
        }
        break;

      case 'field_collection_item':
      case 'paragraph_item':
        $results['origin_languages'] = $this->getLanguageTranslations();
        break;
    }

    return $results;
  }

  /**
   * Get the origin roles.
   *
   * @return array
   *   The roles.
   */
  public function getOriginRoles(): array {
    $query = $this->select('role', 'r')
      ->fields('r', ['name'])
      ->fields('r', ['rid']);
    $roles = $query->execute()->fetchAll();
    $roles_array = [];
    foreach ($roles as $rol) {
      $roles_array[$rol['rid']] = $rol['name'];
    }

    return $roles_array;
  }

  /**
   * Get the destiny paragraph bundles.
   *
   * @return array
   *   The paragraph bundles.
   */
  public function getParagraphBundles(): array {
    $paragraph_types = $this->entityTypeManager->getStorage('paragraphs_type')->loadMultiple();
    $paragraph_types_array = [];
    foreach ($paragraph_types as $paragraph_type) {
      $paragraph_types_array[$paragraph_type->id()] = $paragraph_type->label();
    }
    return $paragraph_types_array;
  }

  /**
   * Read the origin field collection items.
   *
   * @return \Drupal\migrate_wizard\Entity\MWDatabase
   *   The current database.
   */
  public function readOriginFieldCollectionItems() {
    $query = $this->getFieldCollectionItemsQuery();
    $results = $query->execute()->fetchAll();

    foreach ($results as $result) {
      // Check if entity exists.
      $entity = $this->entityTypeManager->getStorage('origin_field_collection_item')->load($result['field_name'] . '_' . $this->mwDatabase->get('id'));

      if (!$entity) {
        $entity = $this->entityTypeManager->getStorage('origin_field_collection_item')->create([
          'id' => $result['field_name'] . '_' . $this->mwDatabase->get('id'),
          'id_origin' => $result['field_name'],
          'database' => $this->mwDatabase->get('id'),
          'use_env' => $this->mwDatabase->get('use_env'),
          'key' => $this->mwDatabase->get('key'),
          'target' => $this->mwDatabase->get('target'),
          'label' => $result['field_name'],
          'description' => $result['field_name'],
          'source_type' => 'sql',
          'module' => 'migrate_wizard',
          'shared_configuration' => 0,
        ]);
        $entity->save();
      }
    }

    return $this->mwDatabase;
  }

  /**
   * Read the origin medias (mimetypes)
   *
   * @return \Drupal\migrate_wizard\Entity\MWDatabase
   *   The current database.
   */
  public function readOriginMedias() {
    $results = $this->getMediasQuery($this->mwDatabase->get('key'));

    foreach ($results as $result) {
      $filemime = str_replace('/', '_', $result->getSourceProperty('filemime'));
      $entity = $this->entityTypeManager->getStorage('origin_media')->load($filemime . '_' . $this->mwDatabase->get('id'));

      if (!$entity) {
        $entity = $this->entityTypeManager->getStorage('origin_media')->create([
          'id' => $filemime . '_' . $this->mwDatabase->get('id'),
          'id_origin' => $filemime,
          'database' => $this->mwDatabase->get('id'),
          'use_env' => $this->mwDatabase->get('use_env'),
          'host' => $this->mwDatabase->get('host'),
          'username' => $this->mwDatabase->get('username'),
          'password' => $this->mwDatabase->get('password'),
          'key' => $this->mwDatabase->get('key'),
          'target' => $this->mwDatabase->get('target'),
          'label' => $filemime,
          'description' => $filemime,
          'source_type' => 'sql',
          'filemime' => $filemime,
          'module' => 'migrate_wizard',
          'shared_configuration' => ['destiny_type' => 'media'],
        ]);
        $entity->save();
      }
    }

    return $this->mwDatabase;
  }

  /**
   * Read the origin nodes.
   *
   * @return \Drupal\migrate_wizard\Entity\MWDatabase
   *   The current database.
   */
  public function readOriginNodes() {
    $results = $this->getNodeTypesQuery($this->mwDatabase->get('key'));

    foreach ($results as $result) {
      $entity = $this->entityTypeManager->getStorage('origin_node')->load($result->getSourceProperty('type') . '_' . $this->mwDatabase->get('id'));

      if (!$entity) {
        $entity = $this->entityTypeManager->getStorage('origin_node')->create([
          'id' => $result->getSourceProperty('type') . '_' . $this->mwDatabase->get('id'),
          'id_origin' => $result->getSourceProperty('type'),
          'database' => $this->mwDatabase->get('id'),
          'use_env' => $this->mwDatabase->get('use_env'),
          'host' => $this->mwDatabase->get('host'),
          'username' => $this->mwDatabase->get('username'),
          'password' => $this->mwDatabase->get('password'),
          'key' => $this->mwDatabase->get('key'),
          'target' => $this->mwDatabase->get('target'),
          'label' => $result->getSourceProperty('name'),
          'description' => $result->getSourceProperty('description'),
          'source_type' => 'sql',
          'module' => 'migrate_wizard',
          'shared_configuration' => 0,
        ]);
        $entity->save();
      }
    }

    return $this->mwDatabase;
  }

  /**
   * Read the origin paragraphs.
   *
   * @return \Drupal\migrate_wizard\Entity\MWDatabase
   *   The current database.
   */
  public function readOriginParagraphs() {
    $results = $this->getParagraphsQuery();

    foreach ($results as $result) {
      // Check if entity exists.
      $entity = $this->entityTypeManager->getStorage('origin_paragraph_item')->load($result->getSourceProperty('bundle') . '_' . $this->mwDatabase->get('id'));

      if (!$entity) {
        $entity = $this->entityTypeManager->getStorage('origin_paragraph_item')->create([
          'id' => $result->getSourceProperty('bundle') . '_' . $this->mwDatabase->get('id'),
          'id_origin' => $result->getSourceProperty('bundle'),
          'database' => $this->mwDatabase->get('id'),
          'use_env' => $this->mwDatabase->get('use_env'),
          'key' => $this->mwDatabase->get('key'),
          'target' => $this->mwDatabase->get('target'),
          'label' => $result->getSourceProperty('field_name'),
          'description' => $result->getSourceProperty('field_name'),
          'source_type' => 'sql',
          'module' => 'migrate_wizard',
          'shared_configuration' => 0,
        ]);
        $entity->save();
      }
    }

    return $this->mwDatabase;
  }

  /**
   * Read the origin vocabularies.
   *
   * @return \Drupal\migrate_wizard\Entity\MWDatabase
   *   The current database.
   */
  public function readOriginVocabularies() {
    $results = $this->getVocabulariesQuery($this->mwDatabase->get('key'));

    foreach ($results as $result) {
      $entity = $this->entityTypeManager->getStorage('origin_vocabulary')->load($result->getSourceProperty('machine_name') . '_' . $this->mwDatabase->get('id'));

      if (!$entity) {
        $entity = $this->entityTypeManager->getStorage('origin_vocabulary')->create([
          'id' => $result->getSourceProperty('machine_name') . '_' . $this->mwDatabase->get('id'),
          'id_origin' => $result->getSourceProperty('machine_name'),
          'database' => $this->mwDatabase->get('id'),
          'use_env' => $this->mwDatabase->get('use_env'),
          'host' => $this->mwDatabase->get('host'),
          'username' => $this->mwDatabase->get('username'),
          'password' => $this->mwDatabase->get('password'),
          'key' => $this->mwDatabase->get('key'),
          'target' => $this->mwDatabase->get('target'),
          'label' => $result->getSourceProperty('name'),
          'description' => $result->getSourceProperty('description'),
          'source_type' => 'sql',
          'module' => 'migrate_wizard',
          'shared_configuration' => 0,
        ]);
        $entity->save();
      }
    }

    return $this->mwDatabase;
  }

  /**
   * Transliterate a string.
   *
   * @param string $input
   *   The input string.
   *
   * @return string|null
   *   The transliterated string.
   */
  public function transliterateString(string $input): ?string {
    $transliterated = $this->transliterator->transliterate($input, LanguageInterface::LANGCODE_DEFAULT, '_');
    $transliterated = mb_strtolower($transliterated);
    $transliterated = preg_replace('@[^a-z0-9_./\s+/]+@', '_', $transliterated);

    return str_replace(' ', '_', $transliterated);
  }

  /**
   * Query for read the field collection items.
   */
  protected function getFieldCollectionItemsQuery() {
    return $this->select('field_config', 'fc')
      ->fields('fc', ['field_name'])
      ->groupBy('fc.field_name')
      ->condition('fc.type', 'field_collection');
  }

  /**
   * Get the medias query.
   *
   * @param string $key
   *   The key.
   *
   * @return mixed
   *   The query.
   */
  protected function getMediasQuery($key) {
    $plugin = static::getSourcePlugin('mw_d7_media_type');
    $plugin->configuration['key'] = $key;

    return $plugin;
  }

  /**
   * Get the node types query.
   *
   * @param string $key
   *   The key.
   *
   * @return mixed
   *   The query.
   */
  protected function getNodeTypesQuery($key) {
    $plugin = static::getSourcePlugin('mw_d7_node_type');
    $plugin->configuration['key'] = $key;

    return $plugin;
  }

  /**
   * Return a query for read the paragraphs.
   *
   * @return mixed
   *   The query.
   */
  protected function getParagraphsQuery() {
    return static::getSourcePlugin('d7_paragraphs_item');
  }

  /**
   * Get the vocabularies query.
   *
   * @param string $key
   *   The key.
   *
   * @return mixed
   *   The query.
   */
  protected function getVocabulariesQuery($key) {
    $plugin = static::getSourcePlugin('d7_taxonomy_vocabulary');
    $plugin->configuration['key'] = $key;

    return $plugin;
  }

  /**
   * Get the language translations.
   *
   * @return array
   *   The language translations.
   */
  private function getLanguageTranslations(): array {
    $default_langcode = $this->getOriginDefaultLangcode();
    $query = $this->select('languages', 'l')
      ->fields('l', ['language'])
      ->condition('l.enabled', '0', '!=')
      ->condition('l.language', $default_langcode, '!=');
    $results_query = $query->execute()->fetchAll();
    $results = [];

    foreach ($results_query as $language) {
      $results[] = $language['language'];
    }

    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function fields(): void {
  }

  /**
   * {@inheritdoc}
   */
  public function getIds(): void {
  }

  /**
   * {@inheritdoc}
   */
  public function query(): void {
  }

}
