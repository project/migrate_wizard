<?php

namespace Drupal\migrate_wizard;

use Drupal\Component\Transliteration\PhpTransliteration;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigManager;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionList;
use Drupal\Core\Serialization\Yaml;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Defines a Service to update nodes.
 */
class MWConfigBuilder {

  /**
   * The configuration object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The configuration manager.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The extension list module.
   *
   * @var \Drupal\Core\Extension\ExtensionList
   */
  protected $extensionListModule;

  /**
   * The field type manager.
   *
   * @var \Drupal\migrate_wizard\FieldTypeMWManager
   */
  protected $fieldTypeMWManager;

  /**
   * The current mw_database.
   *
   * @var \Drupal\migrate_wizard\Entity\MWDatabase
   */
  protected $mwDatabase;

  /**
   * The transliterator.
   *
   * @var \Drupal\Component\Transliteration\PhpTransliteration
   */
  protected $transliterator;

  /**
   * Constructs a new MWConfigBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Extension\ExtensionList $extension_list_module
   *   The extension list module.
   * @param \Drupal\Core\Config\ConfigManager $config_manager
   *   The configuration manager.
   * @param \Drupal\Component\Transliteration\PhpTransliteration $php_transliteration
   *   The transliterator.
   * @param \Drupal\migrate_wizard\FieldTypeMWManager $field_type_mw_manager
   *   The field type manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(
    EntityFieldManagerInterface $entity_field_manager,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    ExtensionList $extension_list_module,
    ConfigManager $config_manager,
    PhpTransliteration $php_transliteration,
    FieldTypeMWManager $field_type_mw_manager,
    RequestStack $request_stack
  ) {
    $this->config = $config_factory->getEditable('migrate_wizard.settings');
    $this->configFactory = $config_factory;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->extensionListModule = $extension_list_module;
    $this->configManager = $config_manager;
    $this->transliterator = $php_transliteration;
    $this->fieldTypeMWManager = $field_type_mw_manager;
    $this->mwDatabase = $request_stack->getCurrentRequest()->get('mw_database');
  }

  /**
   * Builds the migration configuration.
   *
   * @param string $origin_node
   *   The origin node.
   * @param array $array_config
   *   The configuration of the migration.
   */
  public function buildMigrationConfig($origin_node, $array_config) {
    unset($array_config['build_migration_config']);
    $this->buildConfigByLanguage($origin_node, $array_config, $array_config[array_key_first($array_config)]['languages']['default']);

    foreach ($array_config[array_key_first($array_config)]['languages'] as $key => $language) {
      if ($key !== 'default') {
        $this->buildConfigByLanguage($origin_node, $array_config, $array_config[array_key_first($array_config)]['languages']['default'], $array_config[array_key_first($array_config)]['languages'][$language], TRUE);
      }
    }
  }

  /**
   * Builds the migration files configuration.
   */
  public function buildMigrationFilesConfig() {
    $array_config['mw_database'] = $this->mwDatabase->get('id');
    $module_path = $this->extensionListModule->getPath('migrate_wizard');
    $wildcard_yml_config = Yaml::decode(file_get_contents($module_path . '/config_widlcards/migrate_plus.migration.wildcard_files.yml'));
    $field_type_mw = $this->preparePlugin('default_files');
    $field_type_mw->name::getD7MigrationConfig($wildcard_yml_config, $array_config, $this->mwDatabase->get('migration_group'));
    $this->installConfig($wildcard_yml_config);
  }

  /**
   * Builds user the migration files configuration.
   */
  public function buildMigrationUserConfig($origin_node, $array_config) {
    unset($array_config['build_migration_config']);
    $module_path = $this->extensionListModule->getPath('migrate_wizard');
    $wildcard_yml_config = Yaml::decode(file_get_contents($module_path . '/config_widlcards/migrate_plus.migration.wildcard_users.yml'));
    $field_type_mw = $this->preparePlugin('default_user');
    $field_type_mw->name::getD7MigrationConfig($wildcard_yml_config, $array_config, $origin_node, $this->mwDatabase->get('migration_group'), $this->mwDatabase->get('shared_configuration')['user_roles']);
    foreach ($array_config[array_key_first($array_config)] as $origin_field => $data_field) {
      if ((isset($data_field['destiny']) && $data_field['destiny'] !== 'none') && $origin_field !== 'languages') {
        $field_type_mw = $this->preparePlugin($data_field['type']);
        $field_type_mw->name::getD7MigrationConfig($wildcard_yml_config, $origin_field, $data_field, $array_config['destiny_type'], $this->config, $this->entityFieldManager, $this->fieldTypeMWManager);
      }
    }
    $this->installConfig($wildcard_yml_config);
  }

  /**
   * Prepares the tamper plugin.
   *
   * @param string $field_type_mw_id
   *   The id of the field type plugin.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *
   * @return FieldTypeMW|null
   *   The tamper plugin instance or null in case the Tamper plugin could not be
   *   instantiated.
   */
  protected function preparePlugin($field_type_mw_id) {
    return $this->fieldTypeMWManager->createReflectionClass($field_type_mw_id);
  }

  /**
   * Builds the migration files configuration by language.
   */
  private function buildConfigByLanguage($origin_node, $array_config, $default_langcode = NULL, $langcode = NULL, $translation = FALSE) {
    $module_path = $this->extensionListModule->getPath('migrate_wizard');
    $yml_type = $array_config['origin_type'];
    if ($array_config['origin_type'] === 'field_collection') {
      $yml_type = 'field_collection';
    }
    $shared_configuration = $this->mwDatabase->get('shared_configuration');
    $wildcard_yml_config = Yaml::decode(file_get_contents($module_path . "/config_widlcards/migrate_plus.migration.wildcard_{$yml_type}.yml"));
    $field_type_mw = $this->preparePlugin('default_' . $yml_type);
    $field_type_mw->name::getD7MigrationConfig($wildcard_yml_config, $array_config, $origin_node, $this->mwDatabase->get('migration_group'), $default_langcode, $langcode, $translation);
    foreach ($array_config[array_key_first($array_config)] as $origin_field => $data_field) {
      if ((isset($data_field['destiny']) && $data_field['destiny'] !== 'none') && $origin_field !== 'languages') {
        $field_type_mw = $this->preparePlugin($data_field['type']);
        $field_type_mw->name::getD7MigrationConfig($wildcard_yml_config, $origin_field, $data_field, $array_config['destiny_type'], $shared_configuration, $this->entityFieldManager, $this->fieldTypeMWManager, $langcode);
      }
    }
    $this->installConfig($wildcard_yml_config);
  }

  /**
   * Save the configuration of a migration.
   */
  private function installConfig($wildcard_yml_config) {
    if (!\is_array($wildcard_yml_config)) {
      throw new \RuntimeException('Invalid YAML file', 'file');
    }
    $entity_config = $this->configManager->loadConfigEntityByName("migrate_plus.migration.{$wildcard_yml_config['id']}");
    $entity_storage = $this->entityTypeManager->getStorage('migration');
    if ($entity_config) {
      $entity = $entity_storage->updateFromStorageRecord($entity_config, $wildcard_yml_config);
      $entity->save();
    }
    else {
      $entity = $entity_storage->createFromStorageRecord($wildcard_yml_config);
      $entity->save();
    }
  }

}
