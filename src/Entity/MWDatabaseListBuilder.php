<?php

namespace Drupal\migrate_wizard\Entity;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of mw_database entities.
 *
 * @ingroup migrate_wizard
 */
class MWDatabaseListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Database');
    $header['id'] = $this->t('Id');
    $header['key'] = $this->t('Key');
    $header['target'] = $this->t('Target');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $row['label'] = $entity->get('use_env') ? getenv($entity->get('database')) : $entity->get('database');
    $row['id'] = $entity->id();
    $row['key'] = $entity->get('key');
    $row['target'] = $entity->get('target');

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new self(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id())
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity): array {
    $operations = parent::getDefaultOperations($entity);
    $operations['list'] = [
      'title' => $this->t('edit'),
      'weight' => 0,
      'url' => Url::fromRoute(
        'entity.database.database_fields',
        ['id' => $entity->id()]
      ),
    ];

    return $operations;
  }

}
