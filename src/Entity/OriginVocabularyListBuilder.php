<?php

declare(strict_types = 1);

namespace Drupal\migrate_wizard\Entity;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a listing of origin vocabulary entities.
 *
 * @ingroup migrate_wizard
 */
class OriginVocabularyListBuilder extends ConfigEntityListBuilder {

  /**
   * The current mw_database.
   *
   * @var \Drupal\migrate_wizard\Entity\MWDatabase
   */
  protected $currentMwDatabase;

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, RequestStack $request_stack) {
    parent::__construct($entity_type, $storage);
    $this->currentMwDatabase = $request_stack->getCurrentRequest()->get('mw_database');
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Origin Vocabulary');
    $header['machine_name'] = $this->t('Machine Name');
    $header['description'] = $this->t('Description');
    $header['database'] = $this->t('Database');
    $header['source_type'] = $this->t('Source Type');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $entityMwDatabase = $entity->get('use_env') ? getenv($entity->get('database')) : $entity->get('database');
    $row['label'] = $entity->label();
    $row['machine_name'] = $entity->id();
    $row['description'] = $entity->get('description');
    $row['database'] = $entityMwDatabase;
    $row['source_type'] = $entity->get('source_type');

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance($container, $entity_type) {
    return new self(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity): array {
    $operations = parent::getDefaultOperations($entity);
    $operations['list'] = [
      'title' => $this->t('Manage Fields Origin Vocabularies'),
      'weight' => 0,
      'url' => Url::fromRoute(
        'entity.origin_vocabularies.origin_vocabularies_fields',
        [
          'mw_database' => $this->currentMwDatabase->id,
          'origin_bundle' => $entity->id(),
        ]
      ),
    ];

    return $operations;
  }

}
